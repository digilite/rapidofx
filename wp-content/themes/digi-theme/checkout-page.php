<?php
/**
 * Template Name: checkout
 */
?>
<style>

  ul#menu-menu-1 {
    float: right;
  }
</style>
<input class="home_zip" value="<?php echo $_POST['zip']?>" hidden>
<input class="orders" value='<?php echo json_encode($_POST['order']); ?>' hidden>
<input class="shiping_date" value="<?php echo $_POST['shiping_date']?>" hidden>
<input class="shiping_type" value="" hidden>
<input class="shiping_price" value="<?php echo $_POST['shipping_price']?>" hidden>

<?php get_header(); ?>


<?php get_template_part( 'menu' );

global $wp_session;
//session_destroy();
$_SESSION["orders"] = $_POST['order'];
$all = $_POST['order'];
$sum =0;
if(isset($all)){
    foreach ($all as $a){
        $sum += $a['sum'];
    }
}


?>

<?php

if ( is_user_logged_in() ) {

$current_user = wp_get_current_user($current_user->ID);
    ?>
    <input  id="id_user" type="id_user" name="id_user" class="form-control  req" value="<?php echo $current_user->ID;?>" required hidden>

    <?php
} else {?>
    <input  id="id_user" type="id_user" name="id_user" class="form-control  req" value="" required hidden>
<?php }
?>

<div class="overflow-hidden">
    <div class="checkout-page container">
        <div class="checkout-block col-lg-8 col-md-7 col-sm-12 col-xs-12">

            <div class="checkout-form-tab">
                <div class="checkout-title">
                    <h1>Checkout Process</h1>
                    <?php if ( !is_user_logged_in() ) {?> <p>Have an account?<i class="fa fa-lock"></i><a href="<?php echo home_url('login')?>">Log in</a></p><?php } ?>
                </div>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="guest">
                            <div class="form-subtitle">Shipping Address</div>
                                <form id="guest_form"  action="#">
                                <div class="error_block"></div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label >First Name</label>
                                                <input  type="text" name="name" class="form-control name req" value="<?php echo  $current_user->user_login; ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" name="" class="form-control last_name req" value="<?php echo $current_user->user_lastname;?>" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Date of Birth</label>
                                                <input type="date" name="last_name" class="form-control b_day req"  value="<?php echo $current_user->year_of_birth;?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>E-mail</label>
                                                <input  id="email-check" type="email" name="email" class="form-control email req" value="<?php echo  $current_user->user_email?>" <?php if ( is_user_logged_in() ) { echo 'disabled';}?> required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label >Phone</label>
                                                <input type="number" name="phone" class="form-control phone req" value="<?php echo $current_user->phone;?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="text" name="address" class="form-control address req" value="<?php echo $current_user->address;?>" required >
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label >City</label>
                                                <input type="text" name="city" class="form-control city req" value="<?php echo $current_user->city;?>" required>
                                                <i class="fa fa-equals"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Province</label>
                                                  <select name="province" class=" province req">
                                                    <option value="" > </option>
                                                      <?php
                                                      $provinces = array('AB', 'BC', 'MB','NB','NL','NS','ON','PE','QC','SK');

                                                      foreach($provinces as $province){
                                                        if($current_user->province== $province){?>
                                                            <option value="<?php echo $province; ?>" selected> <?php echo $province; ?></option>
                                                       <?php }?>
                                                      <option value="<?php echo $province; ?>"> <?php echo $province; ?></option>
                                                    <?php }?>
                                                    </select>

<!--                                                <input type="text"  name="province" class="form-control province req"  value="--><?php //echo $current_user->province;?><!--" required>-->
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label >Postal Code</label>
                                                <img  class="star-code" src="<?php echo get_template_directory_uri(); ?>/image/star.svg">
                                                <input type="text" name="zip_code" class="form-control postal-code req"   value="<?php echo $_POST['zip'];?>" required disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row address-2">
                                        <div class="col-sm-6 form-subtitle"> Billing Address</div>
                                        <div class="col-sm-6">

                                            <section title="slide">
                                                <!-- .slideThree -->
                                                <div class="slideThree">
                                                    <input type="checkbox" value="None" id="slideThree"/>
                                                    <label for="slideThree"></label>
                                                </div>
                                                <!-- end .slideThree -->
                                            </section>
                                            <span class="checkbox-lable">Same as Shipping Address</span>

                                        </div>
                                    </div>
                                    <div id="billing-form-1">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <input type="text" name="name_billing" class="form-control name_b" value="<?php echo $current_user->name_billing;?>" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Last Name</label>
                                                    <input type="text" name="last_name_billing" class="form-control last_name_b" value="<?php echo $current_user->last_name_billing;?>" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>E-mail</label>
                                                    <input type="email"  name="email_billing" class="form-control email_b" value="<?php echo $current_user->email_billing;?>" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Phone</label>
                                                    <input type="text" name="phone_billing" class="form-control phone_b"  value="<?php echo $current_user->phone_billing;?>" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <input type="text" name="address_billing" class="form-control address_b" value="<?php echo $current_user->address_billing;?>" >
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <input type="text" name="city_billing" class="form-control city_b" value="<?php echo $current_user->city_billing;?>" >
                                                    <i class="fa fa-equals"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                  <label>Province</label>
                                                  <select name="province_billing" class="form-control province_b">
                                                    <option value="" > </option>
                                                   <?php
                                                   foreach($provinces as $province){
                                                    if($current_user->province_billing== $province){?>
                                                    <option value="<?php echo $province; ?>" selected> <?php echo $province; ?></option>
                                                    <?php }?>
                                                    <option value="<?php echo $province; ?>"> <?php echo $province; ?></option>
                                                    <?php }?>
                                                  </select>
<!--                                                 -->
<!--                                                    <input type="text" name="province_billing" class="form-control province_b" value="--><?php //echo $current_user->province_billing;?><!--">-->
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Postal Code</label>
                                                    <img  class="star-code" src="<?php echo get_template_directory_uri(); ?>/image/star.svg">
                                                    <input type="text" name="zip_code_billing" class="form-control postal-code zip_b" value="<?php echo $current_user->zip_code_billing;?>" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </form>

                        </div>
                    </div>
            </div>
          <?php
          $shipping=gerShippingPrice( substr($_POST['zip'],0 ,3));
          ?>
            <div class="options-block">
                <h3>Shipping Options</h3>
                <div class="price-cont">
                    <div class="price-block" data="Same day delivery" data-price="<?=$shipping[0]->same ?>">
                        <h1>$<?=$shipping[0]->same ?></h1>
                        <span>Same day delivery</span>
                        <p>Estimated Delivery - 07/16</p>
                    </div>
                    <div class="price-block" data="Next day delivery" data-price="<?=$shipping[0]->next ?>">
                        <h1>$<?=$shipping[0]->next?></h1>
                        <span>Next day delivery</span>
                        <p>Estimated Delivery - 07/19</p>
                    </div>
                    <div class="price-block" data="Third day delivery"  data-price="<?=$shipping[0]->third ?>">
                        <h1>$<?=$shipping[0]->third?></h1>
                        <span>Third day delivery</span>
                        <p>Estimated Delive7ry - 07/12</p>
                    </div>
                </div>
            </div>

        </div>
        <div class="summary-block col-lg-4 col-md-5 col-sm-12 col-xs-12">
            <div class="summary-form">
                <h4>Summary</h4>
                <div class="finde_sum" data="<?php echo $sum; ?>"><p>Subtotal</p> <span>$<?php echo $sum; ?></span></div>
                <div class="summary_shipping"><p>Shipping</p><span>$<?php echo $_POST['shipping_price']?></span></div>
<!--                <div class=""><p>Tax on Shippping <i class="fa fa-info-circle"></i></p> <span>$100</span></div>-->
                <div class="total-block"><p>Total</p> <span>$</span></div>
            </div>
            <div class="agree-checkbox">
                <label class="check-container">I agree with the <a>Terms and Conditions</a> and Equifax Identity Validation (this is not a credit check).
                    <input id="agree-checkbox" type="checkbox">
                    <span class="checkmark"></span>
                </label>
            </div>
            <div class="summary-submit">
                <div class="make-change"><i class="fa fa-chevron-left"></i>Make changes</div>
                <button  id="pay-guest" class="pay-btn">Pay</button>
                <button  id="pay-reg" class="pay-btn" style="display: none">Pay</button>
            </div>


        </div>
    </div>

</div>

<!-- The Modal -->
<div class="modal  fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Equifax</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="modale-error-block" style="color: red; word-wrap: break-word; "></div>
        <div class="success" style="color: green;"></div>
        <div class="modale-form">
<!--          <form action="#">-->
<!--            <div class="form-horizontal">-->
<!--              <div class="">-->
<!--                <div class="form-group form-inline">-->
<!--               <label for="sin">SIN:</label>-->
<!--                  <input type="number" class="form-control sin_m"  placeholder="SIN" name="sin" required>-->
<!--                  <button type="submit" class="btn btn-default  " id="submit_equifax">Submit</button>-->
<!--                </div>-->
<!--              </div>-->
<!--            </div>-->
<!---->
<!--          </form>-->
        </div>
        <div class="question-block">

        </div>
      </div>
    </div>
  </div>
</div>
<style>

  ul#menu-menu-1 {
    float: right;
  }
</style>
<?php get_footer(); ?>
