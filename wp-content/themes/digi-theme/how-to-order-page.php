<?php
/**
 * Template Name: How to order
 */
?>
<?php get_header(); ?>
<main class="main-content">
    <div class="container">
        <div class="row">
            <div class="sidebar gray-bg col-md-4">
                <div class="sidebar_home_container">
                    <a href="<?php echo home_url();?>"><img  class="logo" src="<?php echo get_template_directory_uri(); ?>/image/new-logo.png"></a>
                    <div class="order-sidebar">
                        <h1>How to Order</h1>
                        <div class="order-title show-block" data-id="1" >1. <?php echo get_field('title')?></div>
                        <div class="order-title show-block" data-id="2" >2. <?php echo get_field('title_2')?></div>
                        <div class="order-title show-block" data-id="3">3. <?php echo get_field('title_3')?></div>
                        <div class="order-title show-block-4" data-id="4">4.<?php echo get_field('title_4')?> </div>
                        <div class="order-title show-block-5" data-id="5">5. <?php echo get_field('title_5')?></div>
                        <div class="contact-us-sidebar">
                            <p>If you have issues with your order don’t hesitate, just contact with us</p>
                            <div class="send-email"><i class="far fa-envelope"></i>Send an email</div>
                        </div>
                    </div>
                    <div class="sidebar_terms">
                        <div class="privacy-link"><a href="<?php echo home_url();?>/privacy-policy">Privacy Policy</a> | <a href="<?php echo home_url();?>/terms-conditions">Terms & Conditions</a></div>
                        <div class="copyright">&copy; <?php echo  get_bloginfo("name"). " " . date("Y"); ?>  Made By <a href="https://digilite.ca/">Digilite</a>.</div>
                    </div>
                </div>
        
            </div>
            <div class="page_container col-md-8 bg-overlay">
                <?php get_template_part( 'menu' );?>
                <div class="home_content home_rate_margin">
                    <div class="reat_select">
                        <div class=" select-group  block-opacity" id="1">
                            <div style="    display: flex;">
                                <img  class="star" src="<?php echo get_template_directory_uri(); ?>/image/star.svg">
                                <select class="form-control">
                                    <option>nrs</option>
                                    <option>nrs</option>
                                    <option>nrs</option>
                                </select>
                            </div>
        
                            <!--                        <input type="email" class="form-control" id="email" placeholder="" name="email">-->
                            <img  class="arrow" style="background-color: white;" src="<?php echo get_template_directory_uri(); ?>/image/arrow_right.svg">
                        </div>
                    </div>
                    <div class="rate-value-block block-opacity"  id="2">
                        <div class="rate-container ">
                            <div>
                                <p>have</p>
                                10000000
                            </div>
                            <div class="flag-block">
                                <img src="<?php echo get_template_directory_uri(); ?>/image/canada_flag.svg">CA
                            </div>
                        </div>
                        <div class="vs-after"></div>
                        <div class="rate-container">
                            <div>
                                <p>have</p>
                                10000000
                            </div>
                            <div class="flag-block">
                                <img src="<?php echo get_template_directory_uri(); ?>/image/british_flag.svg">
                            </div>
                        </div>
                    </div>
                    <div class=" block-opacity">
                        <div class="rate-block2">
                            <div>
                                <i class="fa fa-hourglass"></i>&nbsp;24hr
                            </div>
                            <div class="rate-value">
                                <span>1 CAD = 0.629 GBP</span>
                                <span>1 GBP = 1.591 CAD</span>
                            </div>
                        </div>
                    </div>
        
                    <div class="add-currency  block-opacity">
                        Also traveling to another destination? <span> Add another currency  &nbsp; <i class="fa fa-plus-square"></i></span>
                    </div>

                    <div class="started-block">
                        <div class="description block-opacity ">
                            <?php echo  get_field('description_3')?>
                        </div>
                        <div class=" select-group-2  block-opacity"  id="3">
                            <div style="    display: flex;">
                                <select class="form-control">
                                    <option>nrs</option>
                                    <option>nrs</option>
                                    <option>nrs</option>
                                </select>
                            </div>
        
                            <!--                        <input type="email" class="form-control" id="email" placeholder="" name="email">-->
                            <img  class="equals_icon" src="<?php echo get_template_directory_uri(); ?>/image/equals.svg">
                        </div>
                        <button class=" block-opacity">
                            Get started
                        </button>
                    </div>
                </div>
                <div class="block-4">
                    <img  class="" id="4" src="<?php echo get_template_directory_uri(); ?>/image/block-4.jpg">
                </div>
                <div class="block-5">
                    <div class="block-5-cont">
                        <img  class="order-img-1 block-opacity " src="<?php echo get_template_directory_uri(); ?>/image/left-img.jpg">
                        <img  class="order-img-2" id="5" src="<?php echo get_template_directory_uri(); ?>/image/Tab.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
