<?php
/**
 * Template Name: Who are we
 */
?>
<?php get_header(); ?>
    <main class="main-content">
        <div class="container">
            <div class="row">
                <div class="sidebar gray-bg col-md-4">
                    <div class="sidebar_home_container">
                        <a href="<?php echo home_url();?>"><img class="logo"
                                src="<?php echo get_template_directory_uri(); ?>/image/new-logo.png"></a>
                        <img class="leaf" src="<?php echo get_template_directory_uri(); ?>/image/Group%203.svg">
                        <h1>Foreign Cash, within hours, delivered to your house or office.<br><span>It's easy!</span></h1>
                        <div class="sidebar_terms">
                            <div class="privacy-link"><a href="<?php echo home_url();?>/privacy-policy">Privacy Policy</a> | <a
                                    href="<?php echo home_url();?>/terms-conditions">Terms & Conditions</a></div>
                            <div class="copyright"> &copy; <?php echo  get_bloginfo("name"). " " . date("Y"); ?> Made By <a href="https://digilite.ca/">Digilite</a>.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page_container col-md-8">
                    <?php get_template_part( 'menu' );?>
                    <div class="who-we-cont">
                        <div class="who-we-are-text">
                            <h1>Who Are We</h1>
                            <?php echo  get_field('text')?>
                        </div>
                        <?php
                        $image = get_field('image');
                        $url = $image['url'];
                        ?>
                        <img src="<?php echo  $url; ?>" class="img-thumbnail">
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php get_footer();?>