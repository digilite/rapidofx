<?php
/**
 * Template Name: Rate Watch
 */
?>
<?php get_header(); ?>
    <main class="main-content">
        <div class="container">
            <div class="row">
                <div class="sidebar gray-bg col-md-4">
                    <div class="sidebar_home_container">
                        <a href="<?php echo home_url(); ?>"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/image/new-logo.png"></a>
                        <img class="alarm-icon" src="<?php echo get_template_directory_uri(); ?>/image/alarm.svg">
                        <form class="sidebar-rate-form">
                            <p>Get rate alerts to your email inbox</p>
                            <label class="check-container">Daily email about the CAD to GBP exchange Rate
                                <input type="checkbox">
                                <span class="checkmark"></span>
                            </label>
                            <div class="check-cont">
                                <label class="check-container" style="color: #666;">Email me when 1 CAD goes above&nbsp;
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                                <input type="text">
                            </div>
                            <div class>
                                <label>Email</label>
                                <input type="text" name="email" id="email" class="form-control" placeholder="email" value="">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="Get alerts" class="form-control alerts-button" placeholder="email"
                                    value="Get alerts">
                            </div>
                        </form>
                        <div class="sidebar_terms">
                            <div class="privacy-link"><a href="<?php echo home_url();?>/privacy-policy">Privacy Policy</a> | <a
                                    href="<?php echo home_url();?>/terms-conditions">Terms & Conditions</a></div>
                            <div class="copyright"> &copy; <?php echo  get_bloginfo("name"). " " . date("Y"); ?> Made By <a href="https://digilite.ca/">Digilite</a>.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page_container col-md-8">
                    <?php get_template_part( 'menu' );?>
                    <div class="home_content">
                        <h1>Rate Watch</h1>
                        <div class="watch-rate-block">
                            <div class="currency-block">
                                <div class="currency-cont">
                                    <div class="flag-block">
                                        <img class="currency_from_flag" src="">
                                        <select class="currency-from-watch" data="select-from">
                                            <?php
                                            $currency_from = getCurrencyFrom();
                                            foreach ($currency_from as $currency){
                                                $str = explode(" ", substr(strstr($currency->currency_from , '-') ,1));
                                                $currency_from=  $str['1'];
                                                $img=  $str['3'];
                                                ?>
                                                <option dataa ="<?php echo $img; ?>" data-from="<?php echo $currency->currency_from?>"> <?php  echo $currency_from ;?> </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>

<!--                                    <img src="--><?php //echo get_template_directory_uri(); ?><!--/image/canada_flag.svg">&nbsp;CA-->
                                </div>
                                <div class="vs-after-2"></div>
                                <div class="currency-cont">
                                    <div class="flag-block">
                                        <img class="currency_flag" src="">
                                        <select class="currency-to-watch" data="select-to">
                                            <?php
                                            $currency_from =getCurrencyTo();
                                            foreach ($currency_from as $currency){
                                                $str = explode(" ", substr(strstr($currency->currency_to , '-') ,1));
                                                $currency_to=  $str['1'];
                                                $img=  $str['3'];
                                                ?>
                                                <option data="<?php echo $img; ?>" data-to="<?php echo $currency->currency_to; ?>"> <?php  echo $currency_to;?> </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
<!--                                    <img src="--><?php //echo get_template_directory_uri(); ?><!--/image/british_flag.svg">&nbsp;CA-->
                                </div>
                            </div>
                            <div class="currency-now-block">
                                <div class="now">
                                    <p><i class="fa fa-circle"></i> now</p>
                                </div>
                                <div>
                                    <p id="rate-watch-name">1 Canadian Dollar</p>
                                    <span id="rate-watch">6000</span>
                                </div>
                            </div>
                        </div>
                        <div class="chart-block">
                            <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
                            <style src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.css"></style>
                            <canvas id="myChart" width="400" height="100"></canvas>
                            <script>
                                var canvas = document.getElementById('myChart');
                                var data = {
                                    labels: ["January", "February", "March", "April", "May", "June", "July"],
            
                                    datasets: [{
                                        label: "My First dataset",
                                        fill: false,
                                        lineTension: 0.1,
                                        backgroundColor: "#BA9540",
                                        borderColor: "#BA9540",
                                        borderCapStyle: 'butt',
                                        borderDash: [],
                                        borderDashOffset: 0.0,
                                        borderJoinStyle: 'miter',
                                        // pointBorderColor: "rgba(75,192,192,1)",
                                        // pointBackgroundColor: "#fff",
                                        pointBorderWidth: 1,
                                        pointHoverRadius: 5,
                                        pointHoverBackgroundColor: "#BA9540",
                                        pointHoverBorderColor: "#BA9540",
                                        pointHoverBorderWidth: 2,
                                        pointRadius: 2,
                                        pointHitRadius: 1,
                                        data: [65, 59, 80, 81, 56, 55, 40]
                                    }]
                                };
            
                                var option = {
                                    legend: {
                                        display: false
                                    },
            
                                    showLines: true
                                };
                                var myLineChart = Chart.Line(canvas, {
                                    data: data,
                                    options: option
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>