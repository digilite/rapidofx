<?php
/**
 * Template Name: Terms Page
 */
?>
<?php get_header(); ?>
    <main class="main-content">
        <div class="container">
            <div class="row">
                <div class="sidebar gray-bg col-md-4">
                    <div class="sidebar_home_container">
                        <a href="<?php echo home_url();?>"><img class="logo"
                                                                src="<?php echo get_template_directory_uri(); ?>/image/new-logo.png"></a>
                        <div class="terms-sidebar">
                            <div class="terms-title show-terms-btn" date="1">1. <?php echo get_field('title')?></div>
                            <div class="terms-title show-terms-btn" date="2">2.  <?php echo get_field('title_2')?></div>
                            <div class="terms-title show-terms-btn" date="3">3.  <?php echo get_field('title_3')?></div>
                            <div class="terms-title show-terms-btn" date="4">4. <?php echo get_field('title_4')?></div>
                            <div class="terms-title show-terms-btn" date="5">5. <?php echo get_field('title_5')?></div>
                            <div class="terms-title show-terms-btn" date="6">6. <?php echo get_field('title_6')?></div>
                            <div class="terms-title show-terms-btn" date="7">7. <?php echo get_field('title_7')?></div>
                            <div class="terms-title show-terms-btn" date="8">8. <?php echo get_field('title_8')?></div>
                            <div class="terms-title show-terms-btn" date="9">9. <?php echo get_field('title_9')?></div>
                            <div class="terms-title show-terms-btn" date="10">10. <?php echo get_field('title_10')?></div>
                        </div>
                        <div class="sidebar_terms">
                            <div class="privacy-link"><a href="<?php echo home_url();?>/privacy-policy">Privacy Policy</a> | <a
                                        href="<?php echo home_url();?>/terms-conditions">Terms & Conditions</a></div>
                            <div class="copyright">&copy; <?php echo  get_bloginfo("name"). " " . date("Y"); ?> Made By Digilite.
                            </div>
                        </div>
                    </div>

                </div>
                <div class="page_container col-md-8">
                    <?php get_template_part( 'menu' );?>
                    <div class="terms-cont">
                        <h1>Terms & Conditions</h1>
                        <div class="info-block">
                            <div class="info-title">1. <?php echo get_field('title')?></div>
                            <div class="icon-block btn  show-terms-btn" date="1"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-1">
                            <?php echo get_field('text')?>
                        </div>

                        <div class="info-block">
                            <div class="info-title">2.<?php echo get_field('title_2')?></div>
                            <div class=" btn icon-block show-terms-btn" date="2"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-2">
                            <?php echo get_field('text_2')?>
                        </div>
                        <div class="info-block">
                            <div class="info-title">3. <?php echo get_field('title_3')?></div>
                            <div class="icon-block btn show-terms-btn" date="3"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-3">
                            <?php echo get_field('text_3')?>
                        </div>
                        <div class="info-block">
                            <div class="info-title">4. <?php echo get_field('title_4')?></div>
                            <div class="icon-block btn show-terms-btn" date="4"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-4">
                            <?php echo get_field('text_4')?>
                        </div>
                        <div class="info-block">
                            <div class="info-title">5. <?php echo get_field('title_5')?></div>
                            <div class="icon-block btn show-terms-btn" date="5"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-5">
                            <?php echo get_field('text_5')?>
                        </div>
                        <div class="info-block">
                            <div class="info-title">6. <?php echo get_field('title_6')?></div>
                            <div class="icon-block btn show-terms-btn" date="6"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-6">
                            <?php echo get_field('text_6')?>
                        </div>
                         <div class="info-block">
                            <div class="info-title">7. <?php echo get_field('title_7')?></div>
                            <div class="icon-block btn show-terms-btn" date="7"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-7">
                            <?php echo get_field('text_7')?>
                        </div>
                        <div class="info-block">
                            <div class="info-title">8. <?php echo get_field('title_8')?></div>
                            <div class="icon-block btn show-terms-btn" date="8"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-8">
                            <?php echo get_field('text_8')?>
                        </div>
                        <div class="info-block">
                            <div class="info-title">9. <?php echo get_field('title_9')?></div>
                            <div class="icon-block btn show-terms-btn" date="9"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-9">
                            <?php echo get_field('text_9')?>
                        </div>
                        <div class="info-block">
                            <div class="info-title">10. <?php echo get_field('title_10')?></div>
                            <div class="icon-block btn show-terms-btn" date="10"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-10">
                            <?php echo get_field('text_10')?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
<?php get_footer();?>