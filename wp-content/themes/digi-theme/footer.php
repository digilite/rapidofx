    <?php if (is_page('Blog') || is_page('checkout') || is_single()): ?>
        <footer itemscope itemtype="http://schema.org/WPFooter">
            <div class="container">
                <div class="privacy-link"><a href="<?php echo home_url();?>/privacy-policy">Privacy Policy</a> | <a href="<?php echo home_url();?>/terms-conditions">Terms & Conditions</a></div>
                <div class="copyright">   &copy; <?php echo  get_bloginfo("name"). " " . date("Y"); ?>  Made By <a href="https://digilite.ca/">Digilite</a>.</div>
            </div>
        </footer>
    <?php endif; ?>
    <?php wp_footer(); ?>
	</body>
</html>