<?php get_header(); ?>
<?php get_template_part( 'menu' );?>
<?php
if (have_posts()) :
    while (have_posts()) :
        the_post(); ?>
    <div class="single-img">
        <?php the_post_thumbnail('large' , ['class' => 'img-responsive responsive--full', 'title' => 'Feature image']);?>
    </div>
<div class="container single-post-block ">

			<h1><?php the_title(); ?></h1>
            <hr>

            <div class="single-post-date ">
                <div class="post-date "><?php echo date("d.m.Y", strtotime(get_the_date()));?></div>
            </div>

			<p><?php the_content(); ?></p>

            <div class="next-post">
                <?php  $next_post =get_previous_post();
                if ( is_a( $next_post , 'WP_Post' ) ) : ?>
                  <?php echo get_the_post_thumbnail($next_post->ID, 'small');   ?>
                  <div class="next-post-cont">
                      <h2><?php echo get_the_title( $next_post->ID ); ?></h2>
                     <p> <?php echo wp_trim_words( get_the_content($next_post->ID), 25, '...' ); ?></p>
                      <div class="display-flex">
                          <div class="post-date"><?php echo  get_the_time('d.m.Y', $next_post->ID); ?></div>
                          <div class="post-publish-min">&nbsp; 3 minute read</div>
                      </div>
                      <a class="next-post-link" href="<?php echo get_permalink( $next_post->ID ); ?>">Read More</a>
                  </div>

                <?php endif; ?>
            </div>

</div>
<?php
endwhile;
endif; ?>
<?php get_footer(); ?>
