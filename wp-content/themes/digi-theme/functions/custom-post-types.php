<?php
// ADD CUSTOM POST TYPE IF NEEDED

function custom_post_type_events()
{
  $labels = [
    'name' => _x('Orders', 'post type general name'),
    'singular_name' => _x('Orders', 'post type singular name'),
    'add_new' => _x('Add New', 'Orders'),
    'add_new_item' => __('Add New Orders'),
    'edit_item' => __('Edit Order'),
    'new_item' => __('New Order'),
    'all_items' => __('All Orders'),
    'view_item' => __('View Order'),
    'search_items' => __('Search Orders'),
    'not_found' => __('No Orders found'),
    'not_found_in_trash' => __('No Orders found in the Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'Orders'
  ];

  $args = [
    'labels' => $labels,
    'description' => 'Holds our Events and Event specific data',
    'public' => true,
    'menu_position' => 5,
    'supports' => [
      'title',
      'editor',
      'thumbnail',
      'post-formats',
      'custom-fields'
    ],
    'has_archive' => true
  ];
  register_post_type('rxorders', $args);
}
add_action('init', 'custom_post_type_events');

add_action('admin_init', 'my_admin');
add_action('add_meta_boxes', 'add_rxorders_metaboxes');
function add_rxorders_metaboxes()
{
  add_meta_box(
    'wpt_events_location',
    'Event Location',
    'wpt_events_location',
    'events',
    'side',
    'default'
  );
}
function my_admin()
{
  add_meta_box(
    'options_meta_box',
    'Order Options',
    'display_options_meta_box',
    'rxorders',
    'side',
    'high'
  );
}
add_action('save_post', 'add_meta_fields', 10);

function add_meta_fields($post_id)
{
  if (isset($_POST['post_ID'])) {
    $post_id = $_POST['post_ID'];

    if (isset($_POST['curr_from'])) {
      update_post_meta($post_id, 'curr_from', $_POST['curr_from']);
    }
    if (isset($_POST['sum'])) {
      update_post_meta($post_id, 'sum', $_POST['sum']);
    }
    if (isset($_POST['curr_to'])) {
      update_post_meta($post_id, 'curr_to', $_POST['curr_to']);
    }
    if (isset($_POST['total'])) {
      update_post_meta($post_id, 'total', $_POST['total']);
    }
    if (isset($_POST['shipping_type'])) {
      update_post_meta($post_id, 'shipping_type', $_POST['shipping_type']);
    }
    if (isset($_POST['shipping_price'])) {
      update_post_meta($post_id, 'shipping_price', $_POST['shipping_price']);
    }
    if (isset($_POST['shipping_zone'])) {
      update_post_meta($post_id, 'shipping_zone', $_POST['shipping_zone']);
    }
  }
}
function display_options_meta_box($post)
{
  $curr_from = get_post_meta($post->ID, 'curr_from', true);
  $sum = get_post_meta($post->ID, 'sum', true);
  $curr_to = get_post_meta($post->ID, 'curr_to', true);
  $total = get_post_meta($post->ID, 'total', true);
  $shipping_type = get_post_meta($post->ID, 'shipping_type', true);
  $shipping_price = get_post_meta($post->ID, 'shipping_price', true);
  $shipping_zone = get_post_meta($post->ID, 'shipping_zone', true);
  ?>

  <p>
    <label>From</label></br>
    <input type="text" name="curr_from" value="<?php echo $curr_from; ?>" />
  </p>
  <p>
    <label>SUM</label></br>
    <input type="text" name="sum" value="<?php echo $sum; ?>" />
  </p>
  <p>
    <label>TO</label></br>
    <input type="text" name="curr_to" value="<?php echo $curr_to; ?>" />
  </p>
  <p>
    <label>TOTAL</label></br>
    <input type="text" name="total" value="<?php echo $total; ?>" />
  </p>
  <p>
    <label>SHIPPING TYPE</label></br>
    <input type="text" name="shipping_type" value="<?php echo $shipping_type; ?>" />
  </p>
  <p>
  <label>SHIPPING PRICE</label></br>
    <input type="text" name="shipping_price" value="<?php echo $shipping_price; ?>" />
  </p>
  <p>
    <label>ZONE</label></br>
    <input type="text" name="shipping_zone" value="<?php echo $shipping_zone; ?>" />
  </p>

  <?php
}
function ST4_columns_head($defaults)
{
  $defaults['post_id'] = 'ID';
  $defaults['user_id'] = 'User Name';
  $defaults['curr_from'] = 'From';
  $defaults['sum'] = 'Sum';
  $defaults['curr_to'] = 'To';
  $defaults['total'] = 'Total';
  $defaults['shipping_type'] = 'Shipping Type';
  $defaults['shipping_price'] = 'Shipping Price';
  $defaults['shipping_zone'] = 'Shipping Zone';
  $defaults['status'] = 'Status';
  unset($defaults['title']);
  unset($defaults['date']);
  return array_merge($defaults, array(
    'date' => __('Date')
  ));

  //  return $defaults;
}

function get_order_status($post_ID)
{
  $status = get_field('status', $post_ID);
  if ($status) {
    return $status;
  }
}
function get_order_customer($post_ID)
{
  $user_id = get_post_meta($post_ID, 'user_id', true);
  if ($user_id) {
    return $user_id;
  }
}
function get_order_shipping_zone($post_ID)
{
  $shipping_zone = get_post_meta($post_ID, 'shipping_zone', true);
  if ($shipping_zone) {
    return $shipping_zone;
  }
}
function get_order_shipping_price($post_ID)
{
  $shipping_price = get_post_meta($post_ID, 'shipping_price', true);
  if ($shipping_price) {
    return $shipping_price;
  }
}
function get_order_shipping_type($post_ID)
{
  $shipping_type = get_post_meta($post_ID, 'shipping_type', true);
  if ($shipping_type) {
    return $shipping_type;
  }
}
function get_curr_from($post_ID)
{
  $curr_from = get_post_meta($post_ID, 'curr_from', true);
  if ($curr_from) {
    return $curr_from;
  }
}
function get_order_sum($post_ID)
{
  $sum = get_post_meta($post_ID, 'sum', true);
  if ($sum) {
    return $sum;
  }
}
function get_order_to($post_ID)
{
  $curr_to = get_post_meta($post_ID, 'curr_to', true);
  if ($curr_to) {
    return $curr_to;
  }
}
function get_order_total($post_ID)
{
  $total = get_post_meta($post_ID, 'total', true);
  if ($total) {
    return $total;
  }
}
// SHOW THE FEATURED IMAGE
function ST4_columns_content($column_name, $post_ID)
{
  if ($column_name == 'curr_from') {
    $curr_from = get_curr_from($post_ID);
    if ($curr_from) {
      echo $curr_from;
    }
  }
  if ($column_name == 'sum') {
    $sum = get_order_sum($post_ID);
    if ($sum) {
      echo $sum;
    }
  }
  if ($column_name == 'curr_to') {
    $curr_to = get_order_to($post_ID);
    if ($curr_to) {
      echo $curr_to;
    }
  }
  if ($column_name == 'total') {
    $total = get_order_total($post_ID);
    if ($total) {
      echo $total;
    }
  }
  if ($column_name == 'shipping_type') {
    $shipping_type = get_order_shipping_type($post_ID);
    if ($shipping_type) {
      echo $shipping_type;
    }
  }
  if ($column_name == 'shipping_price') {
    $shipping_price = get_order_shipping_price($post_ID);
    if ($shipping_price) {
      echo $shipping_price;
    }
  }
  if ($column_name == 'shipping_zone') {
    $shipping_zone = get_order_shipping_zone($post_ID);
    if ($shipping_zone) {
      echo $shipping_zone;
    }
  }
  if ($column_name == 'user_id') {
    $user_id = get_order_customer($post_ID);
    if ($user_id) {
      echo $user_id;
    }
  }
  if ($column_name == 'status') {
    $status = get_order_status($post_ID);
    if ($status) {
      echo $status;
    }
  }
  if ($column_name == 'post_id') {
    echo $post_ID;
  }
}
add_filter('manage_rxorders_posts_columns', 'ST4_columns_head');
add_action('manage_posts_custom_column', 'ST4_columns_content', 10, 2);
