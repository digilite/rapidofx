<?php
/* WYSIWYG defaults */
/** change tinymce's paste-as-text functionality */
function paste_as_text($mceInit, $editor_id) {
	//turn on paste_as_text by default
	//NB this has no effect on the browser's right-click context menu's paste!
	$mceInit["paste_as_text"] = true;
	return $mceInit;
}
add_filter("tiny_mce_before_init", "paste_as_text", 1, 2);

/** Set the Attachment Display Settings, This function is attached to the 'after_setup_theme' action hook. */
function default_attachment_display_setting() {
	update_option("image_default_align", "left");
	update_option("image_default_link_type", "none");
	update_option("image_default_size", "large");
}
add_action("after_setup_theme", "default_attachment_display_setting");

// CUSTOM MENUS
function custom_menus() {
	register_nav_menus(
		[
			"primary-menu" => __("Primary Menu"),
			"secondary-menu" => __("Secondary Menu"),
			"footer-menu" => __("Footer Menu"),
		]
	);
}
add_action("init", "custom_menus");


function add_menu_class($ulclass) {
    return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}
add_filter('wp_nav_menu','add_menu_class');

function my_custom_user_fields( $contactmethods ) {
        $contactmethods['user_name'] = 'User Name';
        $contactmethods['year_of_birth'] = 'Year of birth';
        $contactmethods['phone'] = 'Phone';
        $contactmethods['address'] = 'Address';
        $contactmethods['city'] = 'city';
        $contactmethods['province'] = 'Province';
        $contactmethods['zip_code'] = 'Zip_code';
        $contactmethods['name_billing'] = 'Billing Name';
        $contactmethods['last_name_billing'] = 'Billing Last Name';
        $contactmethods['email_billing'] = 'Billing Email';
        $contactmethods['phone_billing'] = 'Billing Phone';
        $contactmethods['address_billing'] = 'Billing Address';
        $contactmethods['city_billing'] = 'Billing City';
        $contactmethods['province_billing'] = 'Billing Province';
        $contactmethods['zip_code_billing'] = 'Billing Zip Code';
        $contactmethods['shipping_type'] = 'Shipping Type';
        $contactmethods['Shipping_date'] = 'Shipping date';
        return $contactmethods;


}
add_filter('user_contactmethods','my_custom_user_fields',10,1);

add_action( 'wp_login_failed', 'my_front_end_login_fail' );  // hook failed login
function my_front_end_login_fail( $username ) {
    $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
    // if there's a valid referrer, and it's not the default log-in screen
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
        if ( !strstr($referrer, '?login=failed') ) {
// Redirect to the login page and append a querystring of login failed
            wp_redirect( $referrer . '?login=failed' );
        } else {
            wp_redirect( $referrer );
        } exit;
//        wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
//        exit;
    }
}


add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );

function wti_loginout_menu_link( $items, $args ) {
    ob_start();
        if (is_user_logged_in()) {
            $items .= '
<li class="profile_btn"><a href="'. home_url('/profile') .'">'. __("My account") .'</a></li>
<li><a class=""  href="'. wp_logout_url() .'">'. __("Log Out") .'</a></li>
 <!--  <li class="dropdown login-menu-button">
            <a class="dropdown-toggle" data-toggle="dropdown"  href="'. wp_logout_url() .'">'. __("Log Out") .'</a>
            <ul class="dropdown-menu">
            <li><a href="'. home_url('/profile') .'">'. __("My account") .'</a></li>
            <li> <a class=""  href="'. wp_logout_url() .'">'. __("Log Out") .'</a></li>
            </ul>
            </li>*/ -->
            ';
        } else {
            $items .= '<li class="right login-menu-button "><a class="" href="'. home_url('/login') .'">'. __("Log In") .'</a></li>
';
        }
    ob_end_clean();
    return $items;
}


add_filter('admin_init', 'my_general_settings_register_fields');

function my_general_settings_register_fields()
{
    register_setting('general', 'my_field', 'esc_attr');
    add_settings_field('my_field', '<label for="my_field">'.__('Email for order notification' , 'my_field' ).'</label>' , 'my_general_settings_fields_html', 'general');
}

function my_general_settings_fields_html()
{
    $value = get_option( 'my_field', '' );
    echo '<input type="text" id="my_field" name="my_field" value="' . $value . '" />';
}
function register_my_session(){
    if( !session_id() ) {
        session_start();
    }
}

add_action('init', 'register_my_session');
