<?php
function enqueue_assets()
{
  //	wp_enqueue_style("bootstrap-style", "//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css", [], "", "all");
  //	wp_style_add_data( 'bootstrap-style', array( 'integrity', 'crossorigin' ) , array( 'sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T', 'anonymous' ) );
  //	wp_enqueue_style("font-awesome-5", "//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css", [], "", "all");
  //	wp_style_add_data( 'font-awesome-5', array( 'integrity', 'crossorigin' ) , array( 'sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU', 'anonymous' ) );
  //	wp_enqueue_style("stylecss", get_stylesheet_uri());

  wp_enqueue_style(
    'font-awesome',
    '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
    '',
    ''
  );
  wp_enqueue_style(
    'muli-font',
    '//fonts.googleapis.com/css?family=Muli:300,400,700,900&display=swap',
    '',
    ''
  );

  wp_enqueue_script(
    'jquery-js',
    '//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
    '',
    '',
    true
  );
  wp_enqueue_script(
    'main-jjs',
    get_template_directory_uri() . '/js/main.js',
    '',
    '',
    true
  );
  //    wp_localize_script( 'main-jjs', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

  wp_enqueue_style(
    'bootstrap-style',
    '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
    [],
    '',
    'all'
  );
  wp_style_add_data(
    'bootstrap-style',
    array('integrity', 'crossorigin'),
    array(
      'sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T',
      'anonymous'
    )
  );
  wp_enqueue_style(
    'font-awesome-5',
    '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css',
    [],
    '',
    'all'
  );
  wp_style_add_data(
    'font-awesome-5',
    array('integrity', 'crossorigin'),
    array(
      'sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU',
      'anonymous'
    )
  );
  wp_enqueue_style(
    'main-styles',
    get_template_directory_uri() . '/css/style.css',
    '',
    ''
  );
  wp_enqueue_style(
    'media-styles',
    get_template_directory_uri() . '/css/media.css',
    '',
    ''
  );

  if (is_page('contact-us')) {
    wp_enqueue_script(
      'google-api',
      '//maps.googleapis.com/maps/api/js?key=AIzaSyAG1EChKTqRIrN8-DmjmlsqUYUB8ByPlFw',
      '',
      '',
      true
    );
    wp_enqueue_script(
      'map-settings-js',
      get_template_directory_uri() . '/js/google-maps-settings.js',
      '',
      '',
      true
    );
  }
  wp_enqueue_script('jquery');

  wp_enqueue_style(
    'owl-carousel-css',
    get_template_directory_uri() . '/css/owl.carousel.min.css',
    '',
    '',
    false
  );
  wp_enqueue_script(
    'owl-carousel-js',
    get_template_directory_uri() . '/js/owl.carousel.min.js',
    '',
    '',
    true
  );

  //wp_enqueue_script("jquery-js", "//code.jquery.com/jquery-3.3.1.slim.min.js", "", "", true);
  //wp_script_add_data( 'jquery-js', array( 'integrity', 'crossorigin' ) , array( 'sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo', 'anonymous' ) );
  wp_enqueue_script(
    'popper-js',
    '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js',
    '',
    '',
    true
  );
  wp_script_add_data(
    'popper-js',
    array('integrity', 'crossorigin'),
    array(
      'sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1',
      'anonymous'
    )
  );
  wp_enqueue_script(
    'bootstrap-4-bundle',
    'https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js',
    '',
    '?'

  );
  wp_enqueue_script(
    'bootstrap-4',
    '//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
    '',
    '',
    true
  );
  wp_script_add_data(
    'bootstrap-4',
    array('integrity', 'crossorigin'),
    array(
      'sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM',
      'anonymous'
    )
  );

  wp_enqueue_script(
    'functions',
    get_template_directory_uri() . '/js/functions.js',
    '',
    '',
    true
  );
  wp_localize_script('functions', 'wp_var', [
    'ajax_url' => admin_url('admin-ajax.php')
  ]);
}
add_action('wp_enqueue_scripts', 'enqueue_assets');
