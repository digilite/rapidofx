<?php
/**
 * Template Name:Login
 */
?>
<?php get_header(); ?>
    <main class="main-content">
        <div class="container">
            <div class="row">
                <div class="sidebar gray-bg col-md-4">
                    <div class="sidebar_home_container">
                        <a href="<?php echo home_url();?>"> <img class="logo"
                                src="<?php echo get_template_directory_uri(); ?>/image/new-logo.png"></a>
                        <?php
                        $args = array(
                            'echo' => true,
                            'redirect' => home_url(),
                            'form_id' => 'loginform',
                            'label_username' => __( 'Username' ),
                            'label_password' => __( 'Password' ),
                            'label_remember' => __( 'Remember Me' ),
                            'label_log_in' => __( 'Log In' ),
                            'id_username' => 'user_login',
                            'id_password' => 'user_pass',
                            'id_remember' => 'rememberme',
                            'id_submit' => 'wp-submit',
                            'remember' => true,
                            'value_username' => NULL,
                            'value_remember' => false
                        );
                        $args = array( 'redirect' => site_url() );
                        ?>

                        <div class="login-form">
                            <div class="login-form-header">
                                <div class="form-name">
                                    <h1>Login</h1>
                                </div>
                                <div class="register-link">
                                    Haven’t account?<a href="#" id="register-form-link">Register</a>
                                </div>
                                <div class="login-link">
                                    Have an account?<a href="#" id="login-form-link">Login</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">

                                    <form name="loginform" id="loginform" action="<?php echo site_url();?>/wp-login.php" method="post" style="display: block;">
                                        <div class="error_block">
                                            <?php if(isset($_GET['login']) && $_GET['login'] == 'failed')
                                            { ?>
                                            <div class="error">
                                                <p>Login failed, wrong Username or Password</p>
                                            </div>

                                            <?php } ?>
                                        </div>
                                        <div class="form-group">
                                            <label  for="user_login">Email</label>
                                            <input  type="text" name="log" id="user_login" class="input form-control" value="" size="20" tabindex="10"
                                                placeholder="Username" >
                                        </div>
                                        <div class="form-group">
                                            <label for="user_pass">Password</label>
                                            <input  type="password" name="pwd" id="user_pass" class="input form-control" value="" size="20" tabindex="20"
                                                placeholder="Password">
                                        </div>
                                        <div class="login-submit-block">
                                            <label class="check-container">Remember me
                                                <input type="checkbox"  name="rememberme"  id="rememberme" value="forever" tabindex="90" >
                                                <span class="checkbox-login"></span>
                                            </label>
                                            <input type="submit" name="wp-submit" id="wp-submit" tabindex="4"
                                                class="form-control btn btn-login" value="Log In">
                                            <input type="hidden" name="redirect_to" value="<?php echo site_url();?>">
                                        </div>


                                    </form>


                                    <form id="register-form"  action="" role="form" style="display: none;">
                                        <div class="error_block"></div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" name="email" id="email" tabindex="1" class="form-control" pattern="[^ @]*@[^ @]*"
                                                   placeholder="email" value="" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Date of Birth</label>
                                            <input type="date" name="date" id="date" tabindex="1" class="form-control"
                                                placeholder="date" value="" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" name="password" id="password" tabindex="2"
                                                class="form-control" placeholder="Password" size="8" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Repeat password</label>
                                            <input type="password" name="confirm-password" id="confirm-password" size="8"
                                                tabindex="2" class="form-control" placeholder="Confirm Password" required>
                                        </div>
                                        <div class="login-submit-block">
                                            <label class="check-container">Remember me
                                                <input  class="remember_me" type="checkbox">
                                                <span class="checkbox-login"></span>
                                            </label>
                                            <input type="submit" name="register-submit" id="register-submit" tabindex="4"
                                                class="form-control btn btn-register" value="Register">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar_terms">
                            <div class="privacy-link"><a href="<?php echo home_url();?>/privacy-policy">Privacy Policy</a> | <a
                                    href="<?php echo home_url();?>/terms-conditions">Terms & Conditions</a></div>
                            <div class="copyright"> &copy; <?php echo  get_bloginfo("name"). " " . date("Y"); ?> Made By
                                <a href="https://digilite.ca/">Digilite</a>.</div>
                        </div>
                    </div>
                </div>
                <div class="page_container col-md-8">
                    <?php get_template_part( 'menu' );?>
                    <div class="login-cont">
                        <img class="img-thumbnail" src="<?php echo get_template_directory_uri(); ?>/image/login_img.png">
                        <h1>Foreign Cash, within hours, delivered to your house or office.<span>Better Rates!</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>
