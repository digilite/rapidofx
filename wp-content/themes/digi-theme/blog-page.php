<?php
/**
 * Template Name: Blog
 */

?>
<?php get_header(); ?>
<?php get_template_part('menu');?>
<div class="container" >
    <h1 class="page-title col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php echo get_the_title();?></h1>
    <div id="blog" class="row">
    <?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array('post_type' => 'post', 'paged' => $paged);
    query_posts($args);
    if( have_posts() ) : while( have_posts() ) : the_post();
        ?>
        <div class=" col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="blog-cont">
                <div  class="blog-img"> <?php the_post_thumbnail('' , array('class' => 'img-responsive')); ?></div>
                <div class="post-cont">
                    <div class="blog-title"> <?php the_title(); ?></div>
                    <div class="blog-text">
                        <p><?php
                            $content = get_the_content();
                            echo wp_trim_words( get_the_content(), 23, '...' );
                            ?></p>
                    </div>
                    <div class="display-flex">
                        <div class="post-date"><?php echo date("d.m.Y", strtotime(get_the_date()));?></div>
                        <div class="post-publish-min">&nbsp; 3 minute read</div>
                    </div>
                    <div class="read-more-block"><a href="<?php the_permalink(); ?>" class="read-more">Read More</a></div>
                </div>
            </div>

        </div>
    <?php  endwhile; ?>
    <?php endif; wp_reset_query(); ?>
    </div>
</div>
<?php get_footer(); ?>
