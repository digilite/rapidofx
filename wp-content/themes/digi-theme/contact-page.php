<?php
/**
 * Template Name: Contact Page
 */
?>
<?php get_header(); ?>
<main class="main-content">
    <div class="container">
        <div class="row">
            <div class="sidebar gray-bg col-md-4">
                <div class="sidebar_home_container">
                    <a href="<?php echo home_url();?>"><img  class="logo" src="<?php echo get_template_directory_uri(); ?>/image/new-logo.png"></a>
                    <div class="form-contact">
                    <h1>Get in Touch</h1>
                        <?php
                        if (have_posts()) :
                            while (have_posts()) :
                                the_post();
                                the_content();
                             endwhile;
                        endif;?>
                    </div>
                    <div class="sidebar_terms">
                        <div class="privacy-link"><a href="<?php echo home_url();?>/privacy-policy">Privacy Policy</a> | <a href="<?php echo home_url();?>/terms-conditions">Terms & Conditions</a></div>
                        <div class="copyright">&copy; <?php echo  get_bloginfo("name"). " " . date("Y"); ?>  Made By <a href="https://digilite.ca/">Digilite</a>.</div>
                    </div>
                </div>
            </div>
            <div class="page_container col-md-8">
                <?php get_template_part( 'menu' );?>
                <div class="contact-cont">
                    <div class="contact-us">
                        <h2>Contacts</h2>
                        <div class="contact-field">
                            <i class="fa fa-map-marker-alt"></i>
                            <span><?php echo get_field('address');?></span>
                        </div>
                        <div class="contact-field">
                            <i class="fa fa-phone-alt"></i>
                            <span>
                                <?php echo get_field('phone');?>
                            </span>
                        </div>
                        <div class="contact-field">
                            <i class="fa fa-envelope"></i>
                            <span>
                                 <?php echo get_field('email');?>
                            </span>
                        </div>
                        <div class="contact-field">
                            <div class="social-icon">
                                <a href="<?php echo get_field('facebook');?>"><i class="fab fa-facebook-f"></i></a>
                            </div>
                            <div class="social-icon">
                                <a href="<?php echo get_field('instagram');?>"><i class="fab fa-instagram"></i></a>
                            </div>
                            <div class="social-icon">
                                <a href="<?php echo get_field('linkedin');?>"><i class="fab fa-linkedin-in"></i></a>
                            </div>
    
                        </div>
    
                    </div>
                    <div class="map">
                        <img src="<?php echo get_template_directory_uri(); ?>/image/map.jpg" class="img-thumbnail">
                    </div>
    
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer();?>