<?php get_header(); ?>
<main class="main-content">
  <div class="container">
    <div class="row">
      <div class="sidebar gray-bg col-md-4 col-sm-12 col-xs-12">
        <div class="sidebar_home_container">
          <a href="<?php echo home_url(); ?>">
            <img class="logo"
                 src="<?php echo get_template_directory_uri(); ?>/image/new-logo.png">
          </a>
          <img class="leaf"
               src="<?php echo get_template_directory_uri(); ?>/image/Group%203.svg">
          <h1>Foreign Cash, within hours, delivered to your house or
            office.</h1>
          <h4>It's easy!</h4>
          <div class="sidebar_terms">
            <div class="privacy-link"><a
                href="<?php echo home_url(); ?>/privacy-policy">Privacy
                Policy</a> | <a
                href="<?php echo home_url(); ?>/terms-conditions">Terms &
                Conditions</a></div>
            <div class="copyright">&copy; <?php echo get_bloginfo('name') .' '.date('Y'); ?> Made By Digilite.
            </div>
          </div>
        </div>
      </div>
      <div class="page_container col-md-8 col-sm-12 col-xs-12">
        <?php get_template_part('menu'); ?>
        <div class="home_content home_rate_margin">

          <form method="post" action="checkout" name="home_form">
            <div class="reat_select">
              <div class="select-group">
                <div style="display: flex;">
                  <img class="star"
                       src="<?php echo get_template_directory_uri(); ?>/image/star.svg">
                  <?php
                  $zipcodes = getZipCode();
                  foreach ($zipcodes as $code) {
                    $arr[] = $code->zip_code;
                  }
                  ?>
                  <input type="text" name="zip" id="zip" value="" required>
                  <input name="zip-arr" id="zip-arr" value='<?php echo json_encode($arr); ?>' hidden>
                </div>
                <!-- <input type="email" class="form-control" id="email" placeholder="" name="email"> -->
                <img class="arrow"
                     src="<?php echo get_template_directory_uri(); ?>/image/arrow_right.svg">
              </div>
            </div>
            <?php
            global $wp_session;
            if (isset($_SESSION['orders'])) { ?>
              <div class="cont-block">
                <?php
                $i = 0;
                foreach ($_SESSION['orders'] as $key => $order) { ?>
                  <input class="ivalue" value="<?php echo count($order) - 1; ?>" hidden>
                  <div id="rate-block<?php if ($i != 0) {echo '-' . $i;} ?>" class="rate">
                    <div class="rate-value-block">
                      <div class="delete-btn "><i class="fa fa-times"></i></div>
                      <div class="rate-container " data="rate-container">
                        <div style="width: 50%">
                          <p>I have</p>
                          <input class="sum"
                                 name="order[<?php echo $i; ?>][sum]"
                                 type="text" style="width: 100%"
                                 value="<?php echo $order['sum']; ?>" required>
                        </div>
                        <div class="flag-block">
                          <img class="currency_from_flag"
                               src="https://www.countryflags.io/<?php echo $order['cfrom']; ?>/flat/24.png">
                          <select class="from-select"
                                  name="order[<?php echo $i; ?>][cfrom]"
                                  data="select-from<?php if (
                                    $i != 0
                                  ) {
                                    echo '-' . $i;
                                  } ?>" value="<?php echo $order['cfrom']; ?>">
                            <?php
                            $currency_from = getCurrencyFrom();
                            foreach (
                              $currency_from
                              as $currency
                            ) {

                              $str = explode(
                                ' ',
                                substr(
                                  strstr(
                                    $currency->currency_from,
                                    '-'
                                  ),
                                  1
                                )
                              );
                              $currency_from =
                                $str['1'];
                              $img = $str['3'];
                              ?>
                              <option dataa="<?php echo $img; ?>"
                                      data-from="<?php echo $currency->currency_from; ?>"> <?php echo $currency_from; ?> </option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="vs-after"></div>
                      <div class="rate-container" data="rate-container">
                        <div style="width: 50%">
                          <p>I want</p>
                          <input class="have-to<?php if ($i != 0) {echo '-' . $i;} ?>" id="have-to"
                                 name="order[<?php echo $i; ?>][total]"
                                 data="input"
                                 value="<?php echo $order['total']; ?>"
                                 readonly>
                        </div>
                        <div class="flag-block">
                          <input value="<?php echo $order['flag_name_to']; ?>"
                                 name="order[<?php echo $i; ?>][flag_name_to]"
                                 class="flag_name_to" hidden>
                          <img class="flag_sess"
                               src="https://www.countryflags.io/<?php echo $order['flag_name_to']; ?>/flat/24.png">
                          <img class="currency_flag" src=""
                               style="display: none;">
                          <select class="currency-to-select"
                                  name="order[<?php echo $i; ?>][cto]"
                                  data="select-to<?php if ($i != 0) {echo '-' . $i;} ?>"
                                  value="<?php echo $order['cto']; ?>">
                            <?php
                            $currency_from = getCurrencyTo();
                            foreach ($currency_from as $currency) {
                              $str = explode(
                                ' ',
                                substr(
                                  strstr(
                                    $currency->currency_to,
                                    '-'
                                  ),
                                  1
                                )
                              );
                              $currency_to =
                                $str['1'];
                              $img = $str['3'];
                              ?>
                              <option data="<?php echo $img; ?>"
                                      data-to="<?php echo $currency->currency_to; ?>" <?php if (
                                $currency_to === $order['cto']
                              ) {
                                echo 'selected';
                              } ?> > <?php echo $currency_to; ?> </option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="rate-block2">
                      <div>
                        <i class="fa fa-hourglass"></i>&nbsp;24hr
                      </div>
                      <div class="rate-value<?php if (
                        $i != 0
                      ) {
                        echo '-' . $i;
                      } ?>" data="rate-value">
                        <span>1 <?php echo $order['cfrom']; ?> =<?php echo $order['total'] / $order['sum']; ?> <?php echo $order['cto']; ?></span>
                      </div>
                    </div>
                  </div>

                  <?php $i++;
                }
                ?>
              </div>
            <?php } else { ?>
              <div class="cont-block">
                <div id="rate-block" class="rate">
                  <div class="rate-value-block">
                    <div class="delete-btn "><i class="fa fa-times"></i></div>
                    <div class="rate-container " data="rate-container">
                      <div style="width: 50%">
                        <p>I have</p>
                        <input class="sum" name="order[0][sum]" type="text"
                               style="width: 100%" required>
                      </div>
                      <div class="flag-block">
                        <img class="currency_from_flag" src="">
                        <select class="from-select" name="order[0][cfrom]"
                                data="select-from">
                          <?php
                          $currency_from = getCurrencyFrom();
                          foreach (
                            $currency_from
                            as $currency
                          ) {

                            $str = explode(
                              ' ',
                              substr(
                                strstr(
                                  $currency->currency_from,
                                  '-'
                                ),
                                1
                              )
                            );
                            $currency_from =
                              $str['1'];
                            $img = $str['3'];
                            ?>
                            <option dataa="<?php echo $img; ?>"
                                    data-from="<?php echo $currency->currency_from; ?>"> <?php echo $currency_from; ?> </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="vs-after"></div>
                    <div class="rate-container" data="rate-container">
                      <div style="width: 50%">
                        <p>I want</p>
                        <input class="have-to" id="have-to"
                               name="order[0][total]" data="input" value=""
                               readonly>
                      </div>
                      <div class="flag-block">

                        <img class="flag_sess" src="" style="display: none;">
                        <img class="currency_flag" src="">

                        <select class="currency-to-select" name="order[0][cto]"
                                data="select-to">
                          <?php
                          $currency_from = getCurrencyTo();
                          foreach (
                            $currency_from
                            as $currency
                          ) {

                            $str = explode(
                              ' ',
                              substr(
                                strstr(
                                  $currency->currency_to,
                                  '-'
                                ),
                                1
                              )
                            );
                            $currency_to =
                              $str['1'];
                            $img = $str['3'];
                            $imgg[] = $str['3'];
                            ?>
                            <option data="<?php echo $img; ?>"
                                    data-to="<?php echo $currency->currency_to; ?>"> <?php echo $currency_to; ?> </option>
                            <?php
                          }
                          ?>
                        </select>
                        <input value="<?php echo $imgg[0]; ?>"
                               name="order[0][flag_name_to]"
                               class="flag_name_to" hidden>
                      </div>
                    </div>
                  </div>
                  <div class="rate-block2">
                    <div>
                      <i class="fa fa-hourglass"></i>&nbsp;24hr
                    </div>
                    <div class="rate-value" data="rate-value">
                      <span>1 CAD = 0.629 GBP</span>
                    </div>
                  </div>
                </div>
              </div>
            <?php }
            ?>
            <div class="add-currency">
              Also traveling to another destination? <span> Add another currency &nbsp; <i
                  class="fa fa-plus-square"></i></span>
            </div>
            <div class="started-block">
              <div class=" select-group-2">
                <div style="    display: flex;
                            ">
                  <select class="rate-value-blockk form-control shipping"
                          name="shiping_date" disabled required>
                    <option></option>
                    <option id="same" data-price="">Same day delivery</option>
                    <option id="next" data-price="">Next day delivery</option>
                    <option id="third" data-price="">Third day delivery</option>
                  </select>
                  <input name="shipping_price" id="shipping_price" value=''
                         hidden>
                </div>

                <!--                        <input type="email" class="form-control" id="email" placeholder="" name="email">-->
                <img class="equals_icon"
                     src="<?php echo get_template_directory_uri(); ?>/image/equals.svg">
              </div>
              <button type="submit" id="home_form_submit">
                Get started
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</main>
<?php get_footer(); ?>


