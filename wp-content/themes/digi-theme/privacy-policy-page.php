<?php
/**
 * Template Name: Privacy Policy Page
 */
?>
<?php get_header(); ?>
    <main class="main-content">
        <div class="container">
            <div class="row">
                <div class="sidebar gray-bg col-md-4">
                    <div class="sidebar_home_container">
                        <a href="<?php echo home_url();?>"><img class="logo"
                                src="<?php echo get_template_directory_uri(); ?>/image/new-logo.png"></a>
                        <div class="policy-sidebar">
                            <div class="policy-title show-btn" date="1"> <?php echo get_field('title')?></div>
                            <div class="policy-title show-btn" date="2"><?php echo get_field('title_2')?>
                            </div>
                            <div class="policy-title show-btn" date="3"><?php echo get_field('title_3')?>
                            </div>
                        </div>
                        <div class="sidebar_terms">
                            <div class="privacy-link"><a href="<?php echo home_url();?>/privacy-policy">Privacy Policy</a> | <a
                                    href="<?php echo home_url();?>/terms-conditions">Terms & Conditions</a></div>
                            <div class="copyright">&copy; <?php echo  get_bloginfo("name"). " " . date("Y"); ?> Made By
                                Digilite.</div>
                        </div>
                    </div>
                </div>
                <div class="page_container col-md-8">
                    <?php get_template_part( 'menu' );?>
                    <div class="privacy-cont">
                        <h1> Privacy Policy</h1>
                        <div class="info-block">
                            <div class="info-title"><?php echo get_field('title')?></div>
                            <div class="icon-block btn  show-btn" date="1"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-1">
                            <?php echo get_field('text')?>
                        </div>
                        <div class="info-block">
                            <div class="info-title"><?php echo get_field('title_2')?></div>
                            <div class=" btn icon-block show-btn" date="2"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-2">
                            <?php echo get_field('text_2')?>
                        </div>
                        <div class="info-block">
                            <div class="info-title"><?php echo get_field('title_3')?></div>
                            <div class="icon-block btn show-btn" date="3"><i class="fa fa-plus"></i></div>
                        </div>
                        <div class="info-text box-3">
                            <?php echo get_field('text_3')?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php get_footer();?>