<?php
/**
 * Template Name: Profile
 */
?>
<?php get_header(); ?>
<?php get_template_part('menu');?>
<div class="container">
    <a href="<?php echo wp_logout_url( home_url()); ?>" title="Logout">Logout</a></br>
    <?php

    if ( is_user_logged_in() ) {
        global $current_user;
        $id = $current_user->ID;
//        echo 'Username: ' . $current_user->user_login . "\n</br>";
        echo 'Username: ' . $current_user->user_login . "\n</br>";
        echo 'User email: ' . $current_user->user_email . "\n</br>";
    } else {
        echo 'login' ;
    }
    ?>
    <?php
    $args = array(
      'post_type'=> 'rxorders',
      'ID'=>$id ,
    'order'    => 'ASC'
    );

    $the_query = new WP_Query( $args );

    global $wpdb;
    $table_name = $wpdb->prefix . 'orders';

    if (isset($_GET['orderdel'])) {
    $del_id = $_GET['orderdel'];
    $wpdb->query("DELETE FROM $table_name WHERE order_id='$del_id'");
    echo "<script>location.replace('admin.php?page=orders');</script>";
    }

    ?>
    <div class="wrap">

        <h2 class="pull-right"> Your Orders</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Currency_from</th>
                    <th>Currency_to</th>
                    <th>Total</th>
                    <th>Sum</th>
                    <th>Shipping Type</th>
                    <td></td>
                </tr>
                </thead>
                <tbody>

                <?php
                if($the_query->have_posts() )  while ( $the_query->have_posts() ) : $the_query->the_post();?>
                <tr>
                  <td><?php echo get_post_meta($post->ID, 'curr_from', true);?></td>
                  <td><?php echo get_post_meta($post->ID, 'curr_to', true);?></td>
                  <td><?php echo get_post_meta($post->ID, 'total', true);?></td>
                  <td><?php echo get_post_meta($post->ID, 'sum', true);?></td>
                  <td><?php echo get_post_meta($post->ID, 'shipping_type', true);?></td>

                  <td>
                    <a class='btn btn-danger' href="<?php echo get_delete_post_link( get_the_ID()); ?>">Delete</a>
                  </td>
                </tr>
              <?php  endwhile;
            ?>

                </tbody>
            </table>
        </div>

    </div>

</div>

<?php get_footer(); ?>
