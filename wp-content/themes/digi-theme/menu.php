
<div class="main-menu <?php if (is_page('Blog') || is_page('checkout') || is_single() || is_page('profile') ){ echo 'container ';}else{ echo 'main-margin-top';} ?>">
    <!-- Navigation -->

    <nav class="navbar navbar-expand-xl">
        <a class="navbar-brand   <?php if ( !is_page('Blog') && !is_page('profile') && !is_page('checkout') && !is_single()){ echo 'hide';}?>" href="<?php echo home_url();?>">
            <img alt="Brand" class="img-fluid  logo-main" src="<?php echo get_template_directory_uri(); ?>/image/new-logo.png">
        </a>
        <button class="navbar-toggler ml-auto " type="button" data-toggle="collapse" data-target="#navbarSupportedContentXL" aria-controls="navbarSupportedContentXL" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars fa-1x"></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarSupportedContentXL">
            <?php
                           wp_nav_menu([
                                   'theme_location' => 'primary-menu',
                                   'menu_class' => ' navbar-nav ml-auto ',
                                   'container' => 'ul',
                                ]); ?>
<!--            --><?php //if ( is_user_logged_in() ) :
//                $current_user = wp_get_current_user();
//                ?>
<!---->
<!--                <li class="login-menu-button"> <a href="--><?php //echo wp_logout_url( get_permalink() ); ?><!--">Logout</a></li>-->
<!--                <li>Username:  --><?php //echo $current_user->user_login; ?><!--</li>-->
<!--            --><?php //else : ?>
<!---->
<!--                <li class="login-menu-button"> <a href="--><?php //echo home_url('login')?><!--">Login</a></li>-->
<!---->
<!--            --><?php //endif;
//
//
//            ?>

        </div>
    </nav>

</div>
