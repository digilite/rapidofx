<?php
include 'WSSoapClient.php';

class EidClient
{
  private $soapUrl = 'https://uat.eidfs.equifax.ca/uru/soap/ut/canadav3?wsdl';
  private $username = '/zangakv';
  private $password = 'oyEy0DZ/TL+ElYkYjFKkTTViObIC+rZx';

  private $client;

  public function __construct()
  {
    $options = [
      'uri' => 'http://schemas.xmlsoap.org/soap/envelope/',
      'style' => SOAP_RPC,
      'use' => SOAP_ENCODED,
      'soap_version' => SOAP_1_1,
      'cache_wsdl' => WSDL_CACHE_NONE,
      'connection_timeout' => 15,
      'trace' => true,
      'encoding' => 'UTF-8',
      'exceptions' => true
    ];

    try {
      $this->client = new WSSoapClient($this->soapUrl, $options);
      $this->client->__setUsernameToken($this->username, $this->password);
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }

  private function __generateArgumentsArray($params)
  {
    $addressLine = !empty($params['addressLine']) ? $params['addressLine'] : '';
    $city = !empty($params['city']) ? $params['city'] : '';
    $province = !empty($params['province']) ? $params['province'] : '';
    $postalCode = !empty($params['postalCode']) ? $params['postalCode'] : '';
    $firstName = !empty($params['firstName']) ? $params['firstName'] : '';
    $lastName = !empty($params['lastName']) ? $params['lastName'] : '';
    $sin = !empty($params['sin']) ? $params['sin'] : '';
    $dateOfBirthDay = !empty($params['dateOfBirthDay'])
      ? $params['dateOfBirthDay']
      : '';
    $dateOfBirthMonth = !empty($params['dateOfBirthMonth'])
      ? $params['dateOfBirthMonth']
      : '';
    $dateOfBirthYear = !empty($params['dateOfBirthYear'])
      ? $params['dateOfBirthYear']
      : '';
    $phoneNumber = !empty($params['phoneNumber']) ? $params['phoneNumber'] : '';
    $language = !empty($params['language']) ? $params['language'] : '';

    $addressField =
      '<v:Address xmlns:v="http://eid.equifax.com/soap/schema/canada/v3" addressType="Current">
                    <v:HybridAddress>
                        <v:AddressLine>' .
      $addressLine .
      '</v:AddressLine>
                        <v:City>' .
      $city .
      '</v:City>
                        <v:Province>' .
      $province .
      '</v:Province>
                        <v:PostalCode>' .
      $postalCode .
      '</v:PostalCode>
                    </v:HybridAddress>
                </v:Address>';

    return [
      'Identity' => [
        'Name' => [
          'FirstName' => $firstName,
          'LastName' => $lastName
        ],
        'Address' => new SoapVar($addressField, XSD_ANYXML),
        'SIN' => $sin,
        'DateOfBirth' => [
          'Day' => $dateOfBirthDay,
          'Month' => $dateOfBirthMonth,
          'Year' => $dateOfBirthYear
        ],
        'PhoneNumber' => [
          'PhoneNumber' => $phoneNumber
        ]
      ],
      'ProcessingOptions' => [
        'Language' => $language
      ]
    ];
  }

  private function __handleResponse($data)
  {
    $result = [
      'status' => 200,
      'data' => [],
      'errors' => []
    ];

    if (!empty($data->FieldChecksFailed)) {
      $result['status'] = 400;
      foreach ($data->FieldChecksFailed as $item) {
        $result['errors'][] = $item;
      }
    }

    if (!empty($data->AssesmentComplete)) {
      $result['status'] = 400;
      foreach ($data->AssesmentComplete->ReasonCode as $item) {
        $result['errors'][] = $item->description;
      }
    }

    if (
      !empty($data->InteractiveQuery) &&
      !empty($data->InteractiveQuery->Question)
    ) {
      $result['data'] = $data->InteractiveQuery->Question;
    }

    return $result;
  }

  public function startTransaction($params)
  {
    try {
      $arguments = $this->__generateArgumentsArray($params);
      $data = $this->client->__soapCall('startTransaction', [$arguments]);

      return $this->__handleResponse($data);
    } catch (Exception $e) {
      echo $e->getMessage();
    }

    return null;
  }
}
