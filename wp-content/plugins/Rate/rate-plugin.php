<?php
/**
 * Plugin Name: RapidoFX  Plugin
 * Plugin URI: https://www.rapidofx.com/
 * Description: This is the RapidoFX plugin.
 * Version: 1.0
 * Author: Test
 * Author URI: http://rapidofx.com/
 **/
function your_namespace() {
    wp_enqueue_script( 'login', plugins_url('js/login.js',__FILE__ ),'','',true);
    wp_localize_script( 'login', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    wp_enqueue_script( 'checkout', plugins_url('js/checkout.js',__FILE__ ),'','',true);
    wp_localize_script( 'checkout', 'checkout_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
//    wp_localize_script( 'login', 'my_ajax_getCurency', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

}
add_action( 'wp_enqueue_scripts','your_namespace');
function admin_namespace(){
    // JS
    wp_register_script('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js','','',true);
    wp_enqueue_script('prefix_bootstrap');
    wp_enqueue_style("font-awesome-5", "//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css", [], "", "all");

    // CSS
    wp_register_style('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css','','');
    wp_enqueue_style('prefix_bootstrap');
}
add_action( 'admin_enqueue_scripts','admin_namespace');
function my_menu_pages(){

    add_menu_page('Rates', 'Rates', 'manage_options', 'RapidoFX', 'rate_page' );
//    add_submenu_page( 'RapidoFX', 'Customers', 'Customers', 'manage_options', 'customers', 'customers_page' );
//    add_submenu_page( 'RapidoFX', 'Orders', 'Orders', 'manage_options', 'orders', 'orders_page' );
    add_submenu_page( 'RapidoFX', 'Zip Code', 'Zip Code', 'manage_options', 'zip_code', 'zipPage' );
    add_submenu_page( 'RapidoFX', 'Zone', 'Zone', 'manage_options', 'zone', 'zonePage' );
 }
add_action('admin_menu', 'my_menu_pages');

function auto_redirect_after_logout(){
    wp_redirect( home_url() );
    exit();
}
add_action('wp_logout','auto_redirect_after_logout');



function my_login_redirect( $redirect_to, $request, $user ) {

    //is there a user to check?
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {

        // check for admins
        if ( in_array( 'administrator', $user->roles ) ) {
            // redirect them to the default place
            return admin_url();
        }
        else {
            return home_url('/profile');
        }
    }
    else {
        return $redirect_to;
    }
}
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );


function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}
add_action('after_setup_theme', 'remove_admin_bar');


function check_username(){
    $email =  sanitize_email( $_POST['mail']);

    $exists = email_exists($email);
    if ( $exists ){
        $response = 'That E-mail is registered';
        echo $response;
    }

    die();
}
add_action( 'wp_ajax_check_username', 'check_username' );
add_action('wp_ajax_nopriv_check_username', 'check_username');

function check_email(){
    $email =  sanitize_email( $_POST['mail']);

    $exists = email_exists($email);
    if ( $exists ){
        $user = get_user_by( 'email', $email );
        $userId = $user->ID;
        wp_send_json($userId);
    }else{
        wp_send_json('');
    }

    die();
}
add_action( 'wp_ajax_check_email', 'check_email' );
add_action('wp_ajax_nopriv_check_email', 'check_email');

function  checkout_user_guest(){
    $name = $_POST['name'];
    $mail = sanitize_email($_POST['mail']);
    $last_name = $_POST['last_name'];
    $b_day = $_POST['b_day'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $province = $_POST['province'];
    $zip = strtoupper($_POST['zip']);
    $name_b = $_POST['name_b'];
    $last_name_b = $_POST['last_name_b'];
    $email_b = $_POST['email_b'];
    $phone_b = $_POST['phone_b'];
    $address_b = $_POST['address_b'];
    $city_b = $_POST['city_b'];
    $province_b = $_POST['province_b'];
    $zip_b = $_POST['zip_b'];
    $user_id = $_POST['user_id'];

    $home_zip = $_POST['home_zip'];
    $orders = stripslashes( $_POST['orders']);
    $shiping_date = $_POST['shiping_date'];
    $shiping_price = $_POST['shiping_price'];
    $res =json_decode($orders, JSON_UNESCAPED_SLASHES);

    $random_password = wp_generate_password( 12 );
    if($user_id !=''){
        $id =$user_id;

    }else{
        $id = wp_create_user( $mail,$random_password,$mail );
        senduserPassword($mail,$random_password);
        $wp_user_object = new WP_User($id);
        $wp_user_object->set_role('customers');
        var_dump('guest');
    }


    wp_update_user( array(
        'ID' => $id,
        'user_name' => $name,
        'year_of_birth' => $b_day,
        'last_name' => $last_name,
        'first_name' => $name,
        'phone' => $phone,
        'address' => $address,
        'city' => $city,
        'province' => $province,
        'zip_code' => $home_zip,
        'name_billing' => $name_b,
        'last_name_billing' =>$last_name_b,
        'email_billing' => $email_b,
        'phone_billing' => $phone_b,
        'address_billing' => $address_b,
        'city_billing' => $city_b,
        'province_billing' => $province_b,
        'zip_code_billing' => $zip_b,
        'shipping_type' => $shiping_date
    ) );
    global $wpdb;
    $table_name = $wpdb->prefix.'orders';

        foreach($res as $order){

            $curr_from = $order['cfrom'];
            $curr_to =  $order['cto'];
            $total = $order['total'];
            $sum =  $order['sum'];
            $date =  date("Y-m-d H:i:s");
            $order_id = wp_insert_post(array(
              'post_title'=>'order',
              'post_type'=>'rxorders',
              'post_date'=>$date,
              'post_status'=>'publish'
            ));
          $user_info = get_userdata($id);

          intval(add_post_meta($order_id, 'curr_from',$curr_from ,true));
          intval(add_post_meta($order_id, 'curr_to', $curr_to,true));
          intval(add_post_meta($order_id, 'sum', $sum,true));
          intval(add_post_meta($order_id, 'total', $total,true));
          intval(add_post_meta($order_id, 'shipping_type', $shiping_date,true));
          intval(add_post_meta($order_id, 'shipping_price', $shiping_price,true));
          intval(add_post_meta($order_id, 'shipping_zone', $zip,true));
          intval(add_post_meta($order_id, 'user_id',  $user_info->user_login,true));
          update_field('status', 'Instead', $order_id);
            $wpdb->query("INSERT INTO $table_name (customer_id,currency_from,currency_to,shipping_type,shipping_date,total,sum,created_at) VALUES('$id','$curr_from','$curr_to','$shiping_date','$shiping_date','$total','$sum','$date')");
            $mess[] = "Currency from->".$curr_from."  Currency to->".$curr_to." Total=".$total." Sum=".$sum."</br>";

        }
        $orders=str_replace("','", '',implode("','", $mess));
        $user_info= array($name,$last_name,$phone,$address,$city,$province,$home_zip,$shiping_date);

        notificationUser($mail,$orders,$user_info);
        //    Admin email
        notificationAdmin($orders,$user_info);
        unset($_SESSION);
        session_destroy();

        return 'ok';
}
add_action( 'wp_ajax_checkout_user_guest', 'checkout_user_guest' );
add_action('wp_ajax_nopriv_checkout_user_guest', 'checkout_user_guest');

function check_equifax(){
  require_once 'services/EidClient.php';
  $firstName = $_POST['fname'];
  $lastName = $_POST['lname'];
  $addressLine = $_POST['address'];
  $city = $_POST['city'];
  $province = $_POST['province'];
  $postalCode = $_POST['zip'];
  $sin = $_POST['sin'];
  $phoneNumber = $_POST['phone'];
  $language = $_POST['language'];
  $dateOfBirthDay = $_POST['dateOfBirthDay'];
  $dateOfBirthMonth = $_POST['dateOfBirthMonth'];
  $dateOfBirthYear = $_POST['dateOfBirthYear'];

  $params = [
    'firstName' => $firstName ,
    'lastName' => $lastName,
    'addressLine' => $addressLine,
    'city' => $city,
    'province' => $province,
    'postalCode' => $postalCode,
    'sin' => $sin,
    'dateOfBirthDay' =>  $dateOfBirthDay,
    'dateOfBirthMonth' =>  $dateOfBirthMonth ,
    'dateOfBirthYear' =>  $dateOfBirthYear,
    'phoneNumber' => $phoneNumber ,
    'language' =>  $language,
  ];

  $client = new EidClient();
  $data = $client->startTransaction($params);
    wp_send_json($data);

}
add_action( 'wp_ajax_check_equifax', 'check_equifax' );
add_action('wp_ajax_nopriv_check_equifax', 'check_equifax');

function getIQAnswerRequest(){
  require_once 'services/EidClient.php';
  $res=$_POST['res'];
  $transactionId =$res['0']['transactionId'];
  $interactiveQueryId =$res['0']['interactiveQueryId'];
  $questions =$res['0']['questions'];
//foreach ($res['0']['questions'] as $val){
//  echo $val['questionId'].',';
//  echo $val['answerId'] .'</br>';
//}

  $params = [
    'transactionId' => $transactionId ,
    'interactiveQueryId' => $interactiveQueryId ,
    'questions' => $questions ,
  ];

  $client = new EidClient();
  $data = $client->interactiveQueryResponse($params);
  $score=$data->AssesmentComplete->AtomicScores->SimpleInteractiveQueryScore;
  wp_send_json($score);

}
add_action( 'wp_ajax_getIQAnswerRequest', 'getIQAnswerRequest' );
add_action('wp_ajax_nopriv_getIQAnswerRequest', 'getIQAnswerRequest');
function notificationUser($mail,$orders,$user_info){
   $message =
    "<html>
    <table width=\"600\" style=\"background-color: #d3dae359;\">
  <tr>
    <td align=\"center\" style=\"background-color: #0dafe6; height: 50px;font-size: 20px;
    text-transform: uppercase;
    color: #fff;
    font-weight: 800;\">thank you for your order</td>
  </tr>
  <tr>
    <td align=\"left\">
  <p><b>Name:</b> $user_info[0] </p>
  <p><b>Last Name:</b> $user_info[1] </p>
  <p><b>Phone:</b> $user_info[2] </p>
  <p><b>Address:</b> $user_info[3] </p>
  <p><b>City:</b> $user_info[4] </p>
  <p><b>Province:</b> $user_info[5] </p>
  <p><b>Zip:</b> $user_info[6] </p>
  <p><b>Shipping type:</b> $user_info[7] </p>
  <h2>Orders</h2>
      $orders
    </td>
  </tr>
</table>
    </html>";
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From:karine@digilite.ca';
    $subject = 'RapidoFX';
    wp_mail($mail,$subject,$message,$headers);
}
function notificationAdmin($orders,$user_info){
  $emails = get_option( 'my_field', '' );
  $admin_email=explode(",", $emails);
  $message =
    "<html>
    <table width=\"600\" style=\"background-color: #d3dae359;\">
  <tr>
    <td align=\"center\" style=\"background-color: #0dafe6; height: 50px;font-size: 20px;
    text-transform: uppercase;
    color: #fff;
    font-weight: 800;\">thank you for your order</td>
  </tr>
  <tr>
    <td align=\"left\">
  <p><b>Name:</b> $user_info[0] </p>
  <p><b>Last Name:</b> $user_info[1] </p>
  <p><b>Phone:</b> $user_info[2] </p>
  <p><b>Address:</b> $user_info[3] </p>
  <p><b>City:</b> $user_info[4] </p>
  <p><b>Province:</b> $user_info[5] </p>
  <p><b>Zip:</b> $user_info[6] </p>
  <p><b>Shipping type:</b> $user_info[7] </p>
  <h2>Orders</h2>
      $orders
    </td>
  </tr>
</table>
    </html>";
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  $headers .= 'From:karine@digilite.ca';
  $subject = 'RapidoFX';
  wp_mail( $admin_email,$subject,$message,$headers);

}
function senduserPassword($mail,$pass){
  $message = "your password is".$pass;
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  $headers .= 'From:karine@digilite.ca';
  $subject = 'RapidoFX';
  wp_mail($mail,$subject,$message,$headers);
}
//user Register
function my_action() {

    $date = $_POST['bday'];
    $useremail  =   sanitize_email( $_POST['mail'] );
    $password   =   $_POST['pass'] ;
    $remember   =  $_POST['checked'] ;

    $user_id = wp_create_user( $useremail,$password,$useremail );
    $wp_user_obj = new WP_User($user_id);
    $wp_user_obj->set_role('customers');
    wp_update_user( array(
        'ID' => $user_id,
        'year_of_birth' => $date
    ) );
    wp_set_auth_cookie( $user_id, $remember );
    var_dump($useremail ,$date , $password);

}
add_action( 'wp_ajax_my_action', 'my_action' );
add_action('wp_ajax_nopriv_my_action', 'my_action');

function getRate(){
    global $wpdb;
    $currency_to= $_POST['currencyTo'];
    $currency_from= $_POST['currencyFrom'];
    $sum= $_POST['sum'];
    $table_name =  $wpdb->prefix . 'rate';
    $response = $wpdb->get_results("SELECT value FROM $table_name WHERE currency_to='$currency_to' and currency_from ='$currency_from'");
    foreach ($response as $res){
        $currency=number_format($res->value * $sum);
    }
    echo $currency;
    die();
}
add_action( 'wp_ajax_getRate', 'getRate' );
add_action('wp_ajax_nopriv_getRate', 'getRate');

function getCurrency(){
    global $wpdb;

    $currency_to= $_POST['currencyTo'];
    $currency_from= $_POST['currencyFrom'];
    $table_name =  $wpdb->prefix . 'rate';
    $str = explode(" ", substr(strstr($currency_to , '-') ,1));
    $str2 = explode(" ", substr(strstr($currency_from , '-') ,1));
    $currency_to_name=  $str['1'];
    $currency_from_name=  $str2['1'];
    $response = $wpdb->get_results("SELECT value FROM $table_name WHERE currency_to='$currency_to' and currency_from ='$currency_from'");
    foreach ($response as $res){
        $currency=$res->value;
    }
    echo '1 '.$currency_from_name.'='.$currency.' '.$currency_to_name;
    die();
}
add_action( 'wp_ajax_getCurrency', 'getCurrency' );
add_action('wp_ajax_nopriv_getCurrency', 'getCurrency');

function getSingleRate(){
    global $wpdb;
    $currency_to= $_POST['currencyTo'];
    $currency_from= $_POST['currencyFrom'];
    $table_name =  $wpdb->prefix . 'rate';
    $str = explode(" ", substr(strstr($currency_to , '-') ,1));
    $str2 = explode(" ", substr(strstr($currency_from , '-') ,1));
    $currency_to_name=  $str['1'];
    $currency_from_name=  $str2['1'];
    $response = $wpdb->get_results("SELECT value FROM $table_name WHERE currency_to='$currency_to' and currency_from ='$currency_from'");
//    var_dump($response->value);
    foreach ($response as $res){
        $currency=$res->value;
    }

    echo $currency;
    die();
}
add_action( 'wp_ajax_getSingleRate', 'getSingleRate' );
add_action('wp_ajax_nopriv_getSingleRate', 'getSingleRate');

function createZipTable(){
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'zip_code';
    $sql = "CREATE TABLE `$table_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(220) DEFAULT NULL,
  `zip_zone` varchar(220) DEFAULT NULL,
  PRIMARY KEY(id)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
  ";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
function createZoneTable(){
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'zone';
    $sql = "CREATE TABLE `$table_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(220) DEFAULT NULL,
  `same` varchar(220) DEFAULT NULL,
  `next` varchar(220) DEFAULT NULL,
  `third` varchar(220) DEFAULT NULL,
  PRIMARY KEY(id)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
  ";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
function createRateTable(){
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'rate';
    $sql = "CREATE TABLE `$table_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_from` varchar(220) DEFAULT NULL,
  `currency_to` varchar(220) DEFAULT NULL,
  `value` float,
  `created_at` date,
  `updated_at` date,
  PRIMARY KEY(id)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
  ";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
function createCustomersTable(){
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix.'customers';
    $sql = "CREATE TABLE `$table_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(220) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `date_birth` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone` varchar(220) DEFAULT NULL,
  `address` varchar(220) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `zip_code` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `name_billing` varchar(220) DEFAULT NULL,
  `last_name_billing` varchar(220) DEFAULT NULL,
  `email_billing` varchar(220) DEFAULT NULL,
  `phone_billing` varchar(220) DEFAULT NULL,
  `address_billing` varchar(220) DEFAULT NULL,
  `city_billing` varchar(100) DEFAULT NULL,
  `province_billing` varchar(100) DEFAULT NULL,
  `zip_code_billing` varchar(100) DEFAULT NULL,
  `shipping` varchar(100) DEFAULT NULL,
  `created_at` date,
    PRIMARY KEY(id)

  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
  ";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
function createOrdersTable(){
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'orders';
    $sql = "CREATE TABLE `$table_name` (
   `order_id` int(11) NOT NULL AUTO_INCREMENT,
   `customer_id` int DEFAULT NULL,
   `currency_from` varchar(220) DEFAULT NULL,
   `currency_to` varchar(220) DEFAULT NULL,
   `shipping_type` varchar(220) DEFAULT NULL,
   `shipping_date` varchar(220) DEFAULT NULL,
   `total` int ,
   `sum` int ,
   `created_at` date,
    PRIMARY KEY(order_id),
    FOREIGN KEY (`customer_id`) REFERENCES ".$wpdb->prefix."customers(`id`) 
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
  ";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
register_activation_hook( __FILE__, 'createZipTable');
register_activation_hook( __FILE__, 'createRateTable');
register_activation_hook( __FILE__, 'createCustomersTable');
register_activation_hook( __FILE__, 'createOrdersTable');
register_activation_hook( __FILE__, 'createZoneTable');

function zipPage(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'zip_code';
    if (isset($_POST['zipsubmit'])) {
        $zip_code = $_POST['zip_code'];
        $zip_zone = $_POST['zip_zone'];
        $wpdb->query("INSERT INTO $table_name(zip_code,zip_zone ) VALUES('$zip_code','$zip_zone')");
        echo "<script>location.replace('admin.php?page=zip_code');</script>";

    }
    if (isset($_GET['zipdel'])) {
        $del_id = $_GET['zipdel'];
        $wpdb->query("DELETE FROM $table_name WHERE id='$del_id'");
        echo "<script>location.replace('admin.php?page=zip_code');</script>";
    }
    ?>
    <div class="wrap">

        <h2 class="pull-right">ZIP CODE</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>zip code</th>
                    <th>zip zone</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <form action="" method="post">
                    <tr>
                        <td><input type="text" hidden disabled></td>
                        <td><input type="text" id="zip_code" name="zip_code"></td>
                        <td><select type="text" id="zip_zone" name="zip_zone">

                                <?php
                                $zons =getZone();
                                foreach ($zons as $zone){?>
                                    <?php?>
                                <option>  <?php echo $zone->zone; ?>
                                    </option>
                                <?php  }  ?>
                            </select></td>
                        <td>
                            <button id="zipsubmit" name="zipsubmit" type="submit" class="btn btn-info btn-sm"><i class="fa fa-plus"></i></button>
                        </td>
                    </tr>
                </form>
                <?php
                $result = $wpdb->get_results("SELECT * FROM $table_name");
                foreach ($result as $print) {
                    echo "
              <tr>
                <td>$print->id</td>
                <td>$print->zip_code</td>
                <td>$print->zip_zone</td>
                 <td>
                    <a href='admin.php?page=zip_code&zipdel=$print->id'>
                    <button type='button' class='btn btn-danger btn-sm'><i class='fa fa-trash-alt'></i></button></a>
                 </td>
              </tr>
            ";
                }
                ?>
                </tbody>
            </table>
        </div>

    </div>
    <?php
}
function zonePage(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'zone';
    if (isset($_POST['zonesubmit'])) {
        $zip_code = $_POST['zone'];
        $same_day_delivery = $_POST['same-day-delivery'];
        $third_day_delivery = $_POST['third-day-delivery'];
        $next_day_delivery = $_POST['next-day-delivery'];
        $wpdb->query("INSERT INTO $table_name(zone,next,same,third) VALUES('$zip_code','$next_day_delivery',' $same_day_delivery','$third_day_delivery')");
        echo "<script>location.replace('admin.php?page=zone');</script>";

    }
    if (isset($_GET['zonedel'])) {
        $del_id = $_GET['zonedel'];
        $wpdb->query("DELETE FROM $table_name WHERE id='$del_id'");
        echo "<script>location.replace('admin.php?page=zone');</script>";
    }
    ?>
    <div class="wrap">

        <h2 class="pull-right">Zone</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Zone</th>
                    <th>Same day delivery</th>
                    <th>Next day delivery</th>
                    <th>Third day delivery</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <form action="" method="post">
                    <tr>
                        <td><input type="text" hidden disabled></td>
                        <td><input type="text" id="zone" name="zone"></td>
                        <td><input type="text" id="same-day-delivery" name="same-day-delivery"></td>
                        <td><input type="text" id="next-day-delivery" name="next-day-delivery"></td>
                        <td><input type="text" id="third-day-delivery" name="third-day-delivery"></td>
                        <td>
                            <button id="zonesubmit" name="zonesubmit" type="submit" class="btn btn-info  btn-sm"><i class="fa fa-plus"></i></button>
                        </td>
                    </tr>
                </form>
                <?php
                $result = $wpdb->get_results("SELECT * FROM $table_name");
                foreach ($result as $print) {
                    echo "
              <tr>
                <td>$print->id</td>
                <td>$print->zone</td>
                <td>$print->same</td>
                <td>$print->next</td>
                <td>$print->third</td>
                 <td>
                    <a href='admin.php?page=zone&zonedel=$print->id'>
                    <button type='button' class='btn btn-danger  btn-sm'><i class='fa fa-trash-alt'></i></button></a>
                 </td>
              </tr>
            ";
                }
                ?>
                </tbody>
            </table>
        </div>

    </div>
    <?php
}

function rate_page()
{
    $currencies = array(

"Canada - CA - CA",
"United States - USD  - US",
"Euro zone - EUR - EU",
"United Kingdom - GBP - GB",
"Japan - JPY - JP",
"Mexico - MXN - MX",
"Australia - AUD - AU",
"Austria - EUR - AT",
"Barbados - BBD -  BB",
"Belgium - EUR - BE",
"Brazil - BRL - BR",
"China - CNY - CN",
"Colombia - COP -  CO",
"Costa Rica - CRC - CR",
"Croatia - HRK - HR",
"Cyprus - EUR - CY",
"Czech Republic - CSK - CZ",
"Denmark - DKK - DK",
"Dominican Republic - DOP - DO",
"Estonia - EUR - EE",
"Finland - EUR - FI",
"France - EUR - FR",
"Germany - EUR - DE",
"Greece - EUR - GR",
"Holland - EUR - NL",
"Hong Kong - HKD - HK",
"Iceland - ISK - IS",
"Ireland - EUR - IE",
"Israel - ILS - IL",
"Italy - EUR - IT",
"Jamaica - JMD - JM",
"Latvia - EUR - LV",
"Lithuania - EUR - LT",
"Luxembourg - EUR - LU",
"Malta - EUR - MT",
"Netherlands - EUR - NL",
"Norway - NOK - NO",
"Portugal - EUR - PT",
"Slovakia - EUR - SK",
"Slovenia - EUR - SI",
"South Korea - KPW - KR",
"Spain - EUR - ES",
"Sweden - SEK - SE",
"Switzerland - CHF - CH",
"United Arab Emirates - AED - AE",
        );

    global $wpdb;
    $table_name = $wpdb->prefix . 'rate';
    if (isset($_POST['newsubmit'])) {
        $currency_from = $_POST['currency_from'];
        $currency_to = $_POST['currency_to'];
        $value = $_POST['value'];
        $created_at =  date("Y-m-d H:i:s");
        $updated_at =  date("Y-m-d H:i:s");
        var_dump($created_at);
        $wpdb->query("INSERT INTO $table_name(currency_from,currency_to,value,created_at,updated_at) VALUES('$currency_from','$currency_to','$value','$created_at','$updated_at')");
        echo "<script>location.replace('admin.php?page=RapidoFX');</script>";

    }
    if (isset($_POST['uptsubmit'])) {
        $id = $_POST['uptid'];
        $currency_from = $_POST['currency_from'];
        $currency_to = $_POST['currency_to'];
        $value = $_POST['value'];
        $updated_at = date("Y-m-d H:i:s");
        $wpdb->query("UPDATE $table_name SET currency_from='$currency_from', currency_to='$currency_to', value='$value',  updated_at='$updated_at'  WHERE id='$id'");
        echo "<script>location.replace('admin.php?page=RapidoFX');</script>";
    }
    if (isset($_GET['del'])) {
        $del_id = $_GET['del'];
        $wpdb->query("DELETE FROM $table_name WHERE id='$del_id'");
        echo "<script>location.replace('admin.php?page=RapidoFX');</script>";
    }
    ?>

    <div class="wrap">
        <div class="table-responsive">
        <h2>RATE</h2>
        <table class=" widefat striped table table-striped table table-sm">
            <thead>
            <tr>
                <th>ID</th>
                <th>currency_from</th>
                <th>currency_to</th>
                <th>value</th>
                <th>created_at</th>
                <th>updated_at</th>
                <th></th>

            </tr>
            </thead>
            <tbody>
            <form action="" method="post">
                <tr>
                    <td><input type="text" hidden disabled></td>
                    <td><select id="currency_from" name="currency_from">
                              <?php   foreach($currencies as $currencie){?>
                            <option>
                                    <?php echo $currencie;?>
                            </option>
                            <?php } ?>
                        </select></td>
                    <td><select id="currency_to" name="currency_to">
                            <?php   foreach($currencies as $currencie){?>
                                <option>
                                    <?php echo $currencie;?>
                                </option>
                            <?php } ?>
                        </select></td>
                    <td><input type="text" id="value" name="value"></td>
                    <td><input type="text" id="created_at" name="created_at" value="<?php echo date('m/d/Y');?>" placeholder="<?php echo date('m/d/Y');?>" disabled></td>
                    <td><input type="date" id="updated_at" name="updated_at" disabled></td>
                    <td>
                        <button id="newsubmit" name="newsubmit" type="submit" class="btn btn-info btn-sm"><i class="fa fa-plus"></i></button>
                    </td>
                    <td></td>
                </tr>
            </form>
            <?php
            $result = $wpdb->get_results("SELECT * FROM $table_name ORDER BY id DESC" );
            foreach ($result as $print) {
            $date_created=date_format(date_create($print->created_at),"d/m/Y");
            $date_updated=date_format(date_create($print->updated_at),"d/m/Y");
                echo "
              <tr>
                <td>$print->id</td>
                <td>$print->currency_from</td>
                <td>$print->currency_to</td>
                <td>$print->value</td>
                <td>$date_created</td>
                <td>$date_updated</td>
                <td class='row'>
                    <a href='admin.php?page=RapidoFX&upt=$print->id'>
                    <button type='button' class='btn btn-warning btn-sm'><i class='fa fa-edit'></i></i></button></a>
                     <a href='admin.php?page=RapidoFX&del=$print->id'>
                    <button type='button' class='btn btn-danger btn-sm'><i class='fa fa-trash-alt'></i></button></a>
                 </td>
              </tr>
            ";
            }
            ?>
            </tbody>
        </table>
        <br>
        <br>
        <?php
        if (isset($_GET['upt'])) {
            $upt_id = $_GET['upt'];
            $result = $wpdb->get_results("SELECT * FROM $table_name WHERE id='$upt_id'");
            foreach ($result as $print) {
                $currency_from = $print->currency_from;
                $currency_to = $print->currency_to;
                $value = $print->value;
                $created_at = $print->created_at;
                $updated_at = $print->updated_at;
            }
            $date=date('m/d/Y');
            echo "
        <table class='wp-list-table widefat striped table table-striped table table-sm'>
          <thead>
            <tr>
                <th>ID</th>
                <th>currency_from</th>
                <th>currency_to</th>
                <th>value</th>
                <th>created_at</th>
                <th>updated_at</th>
                <th></th>
                <th></th>
            
         
            </tr>
          </thead>
          <tbody>
            <form action='' method='post'>
              <tr>
                <td>$print->id <input type='hidden' id='uptid' name='uptid' value='$print->id'></td>
                <td>
                <select class=''  id='currency_from' name='currency_from' value='$print->currency_from'>";
                   foreach($currencies as $currencie){?>
                                <option>
                                  <?php echo  $currencie;?>
                                </option>
                   <?php  }
              echo "
                  </select></td>
                <td><select class=''  id='currency_to' name='currency_to' value='$print->currency_to'>";
                foreach($currencies as $currencie){
                    ?>
                <option>
                    <?php echo  $currencie;?>
                </option>
               <?php  }
               echo "</select></td>
                <td><input type='text' id='value' name='value' value='$print->value'></td>
                <td><input type='text' id='created_at' name='created_at' value='$print->created_at' disabled></td>
                <td><input type='text' id='updated_at' name='updated_at' value='$date' disabled></td>
       
                <td class=''>
                <button id='uptsubmit' name='uptsubmit' type='submit' class='btn btn-warning btn-sm'>
                <i class='fa fa-save'></i></button> 
                </td>
                <td>
                 <a  class='btn btn-dark btn-sm' href='admin.php?page=RapidoFX'>
                <i class='fa fa-times'></i></a>
                </td> 
              
              </tr>
            </form>
          </tbody>
        </table>";
        }
        ?>
    </div>
    </div>

    <?php
}
function customers_page(){

    global $wpdb;
    $table_name = $wpdb->prefix . 'customers';

    if (isset($_GET['customerdel'])) {
        $del_id = $_GET['customerdel'];
        $wpdb->query("DELETE FROM $table_name WHERE id='$del_id'");
        echo "<script>location.replace('admin.php?page=customer');</script>";
    }
    ?>
    <div class="wrap">

        <h2 class="pull-right">Customers</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Last name</th>
                    <th>Date Birth</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>Province</th>
                    <th>zip code</th>
                    <th>Name Billing</th>
                    <th>Last name Billing</th>
                    <th>Email Billing</th>
                    <th>Phone Billing</th>
                    <th>Address Billing</th>
                    <th>City Billing</th>
                    <th>Province Billing</th>
                    <th>zip code Billing</th>
                    <th>Shipping </th>
                    <th>Created at</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $result = $wpdb->get_results("SELECT * FROM $table_name");
                foreach ($result as $print) {
                    echo "
              <tr>
                <td>$print->id</td>
                <td>$print->name</td>
                <td>$print->last_name</td>
                <td>$print->date_birth</td>
                <td>$print->email</td>
                <td>$print->phone</td>
                <td>$print->address</td>
                <td>$print->city</td>
                <td>$print->province</td>
                <td>$print->zip_code</td>
                <td>$print->name_billing</td>
                <td>$print->last_name_billing</td>
                <td>$print->email_billing</td>
                <td>$print->phone_billing</td>
                <td>$print->address_billing</td>
                <td>$print->city_billing</td>
                <td>$print->province_billing</td>
                <td>$print->zip_code_billing</td>
                <td></td>
                <td>$print->created_at</td>
                 <td>
                    <a href='admin.php?page=customers&customerdel=$print->id'>
                    <button type='button' class='btn btn-danger'><i class='fa fa-trash-alt'></i></button></a>
                 </td>
              </tr>
            ";
                }
                ?>
                </tbody>
            </table>
        </div>

    </div>
    <?php
}
function orders_page(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'orders';

    if (isset($_GET['orderdel'])) {
        $del_id = $_GET['orderdel'];
        $wpdb->query("DELETE FROM $table_name WHERE order_id='$del_id'");
        echo "<script>location.replace('admin.php?page=orders');</script>";
    }
    ?>
    <div class="wrap">

        <h2 class="pull-right">Orders</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Customer Id</th>
                    <th>Currency_from</th>
                    <th>Currency_to</th>
                    <th>Total</th>
                    <th>Sum</th>
                    <th>Shipping date</th>
                    <th>Shipping Type</th>
                    <th>Created_at</th>
                    <td></td>
                </tr>
                </thead>
                <tbody>
                <?php
                $result = $wpdb->get_results("SELECT * FROM $table_name");
                foreach ($result as $print) {
                    echo "
              <tr>
                <td>$print->order_id</td>
                <td>$print->customer_id</td>
                <td>$print->currency_from</td>
                <td>$print->currency_to</td>
                <td>$print->total</td>
                <td>$print->sum</td>
                <td>$print->shipping_date</td>
                <td>$print->shipping_type</td>
                <td>$print->created_at</td>
        
                 <td>
                    <a href='admin.php?page=orders&orderdel=$print->order_id'>
                    <button type='button' class='btn btn-danger btn-sm'><i class='fa fa-trash-alt'></i></button></a>
                 </td>
              </tr>
            ";
                }
                ?>
                </tbody>
            </table>
        </div>

    </div>
    <?php
}

function getZone(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'zone';
    $result = $wpdb->get_results("SELECT * FROM $table_name");
    return $result;
}
function getZipCode(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'zip_code';
    $result = $wpdb->get_results("SELECT * FROM $table_name");
    return $result;
}
function getCurrencyFrom(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'rate';
    $result = $wpdb->get_results("SELECT currency_from FROM $table_name GROUP BY currency_from");
    return $result;
}
function getCurrencyTo(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'rate';
    $result = $wpdb->get_results("SELECT currency_to FROM $table_name GROUP BY currency_to");
    return $result;
}
function gerShippingPrice($zip){
  global $wpdb;
  $table_zone = $wpdb->prefix . 'zone';
  $table_name = $wpdb->prefix . 'zip_code';
  $result = $wpdb->get_var("SELECT `zip_zone` FROM $table_name WHERE zip_code='$zip'");
  $shipping_types= $wpdb->get_results("SELECT `same`, `next`, `third` FROM $table_zone WHERE zone='$result'");

  return $shipping_types;

}
function getShipping(){
    $zip_code = $_POST['zip'];
    global $wpdb;
    $table_zone = $wpdb->prefix . 'zone';
    $table_name = $wpdb->prefix . 'zip_code';
    $result = $wpdb->get_var("SELECT `zip_zone` FROM $table_name WHERE zip_code='$zip_code'");
    $shipping_types= $wpdb->get_results("SELECT `same`, `next`, `third` FROM $table_zone WHERE zone='$result'");

    wp_send_json($shipping_types) ;
    die();
}
add_action( 'wp_ajax_getShipping', 'getShipping' );
add_action('wp_ajax_nopriv_getShipping', 'getShipping');
