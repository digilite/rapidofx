(function($) {
  $('.guest').on('click', function(e) {
    $('#pay-guest').css('display', 'block');
    $('#pay-reg').css('display', 'none');
  });
  $('.register').on('click', function(e) {
    $('#pay-reg').css('display', 'block');
    $('#pay-guest').css('display', 'none');
  });

  var a = parseInt($('.shiping_price').val());
  var b = parseInt($('.finde_sum').attr('data'));
  var total = a + b;
  $('.total-block')
    .find('span')
    .text('$' + total);
  $('.price-block').on('click', function(e) {
    $('.price-block').css('border', '1px solid rgba(0, 0, 0, 0.1)');
    $('.price-block')
      .find('span')
      .css('color', '#000000');
    $('.shiping_date').val('');
    $('.shiping_date').val($(this).attr('data'));
    $('.finde_sum').val($(this).attr('data'));
    $('.shiping_price').attr('value', $(this).attr('data-price'));
    $('.summary_shipping')
      .find('span')
      .text('$' + $(this).attr('data-price'));
    $('.total-block')
      .find('span')
      .text('');

    $('.total-block')
      .find('span')
      .text(
        '$' +
          (parseInt($('.shiping_price').val()) +
            parseInt($('.finde_sum').attr('data')))
      );
    $(this).css('border', '1px solid #BA9540');
    $(this)
      .find('span')
      .css('color', '#BA9540');
  });
  $('#billing-form-1').css('display', 'none');
  $('#billing-form-2').css('display', 'none');

  $('.slideThree').on('click', function(e) {
    if ($('#slideThree').is(':checked')) {
      $('#billing-form-1').css('display', 'block');
    } else {
      $('#billing-form-1').css('display', 'none');
    }
    if ($('#slideThree2').is(':checked')) {
      $('#billing-form-2').css('display', 'block');
    } else {
      $('#billing-form-2').css('display', 'none');
    }
  });
  $('#email-check').change(function() {
    var mail = $('#email-check').val();
    $.ajax({
      url: checkout_object.ajax_url,
      type: 'POST',
      data: {
        action: 'check_email',
        mail: mail
      },
      success: function(response) {
        // $('.error_block').append('<div class="error">' + response + '</div>');
        console.log(response);
        $('#id_user').val(response);
      }
    });
  });

  $('#pay-guest').on('click', function(e) {
    var valid = '';
    $('.error_req').remove();
    $('#guest_form')
      .find('.req')
      .each(function() {
        $(this)
          .parents('.form-group')
          .find('.error_req')
          .remove();
        if (!$(this).val()) {
          $(this).addClass('req_error');
          $(this)
            .parents('.form-group')
            .prepend(
              '<div class="error_req">' + 'field is required' + '</div>'
            );
          valid = 'no';
        } else {
          $(this).removeClass('req_error');
          $(this)
            .parents('.form-group')
            .find('.error_req')
            .remove();
        }
      });
    if ($('#agree-checkbox').is(':checked')) {
      console.log('Check box in Checked');
    } else {
      $('.error_block').append(
        "<div class='error_req'>Please accept terms and conditions</div>"
      );
      valid = 'no';
    }
    // if(!$('.shiping_type').val()){
    //     $('.error_block').append("<div class='error_req'>Select shipping type</div>");
    //     valid = 'no';
    // }
    e.preventDefault();
    var checked = '';
    $('.error').remove();
    var mail = $('#email-check').val();
    var name = $('.name').val();
    var last_name = $('.last_name').val();
    var b_day = $('.b_day').val();
    var phone = $('.phone').val();
    var address = $('.address').val();
    var city = $('.city').val();
    var province = $('.province').val();
    var zip = $('.postal-code').val();
    var name_b = $('.name_b').val();
    var last_name_b = $('.last_name_b').val();
    var email_b = $('.email_b').val();
    var phone_b = $('.phone_b').val();
    var address_b = $('.address_b').val();
    var city_b = $('.city_b').val();
    var province_b = $('.province_b').val();
    var zip_b = $('.zip_b').val();
    var user_id = $('#id_user').val();

    var home_zip = $('.home_zip').val();
    var orders = $('.orders').val();
    var shiping_date = $('.shiping_date').val();
    var shiping_price = $('.shiping_price').val();

    // $('.modal').addClass('show');

    // $('.modal').css('display','block');
    // $('.modal').attr('aria-modal', 'true');
    console.log(user_id);
    if (!valid) {

      getQuestion();
    }
  });
  function getQuestion(){
    const fname =   $('.name').val();
    const lname =   $('.last_name').val();
    const address =  $('.address').val();
    const city =  $('.city').val();
    const province =  $('.province').val();
    const zip =  $('.postal-code').val().replace(/\s/g, '');
    // const sin =  $('.sin_m').val();
    const phone =  $('.phone').val();
    const language =  'English';
    const dateOfBirth = $('.b_day').val();
    let arr = dateOfBirth.split('-');
    const dateOfBirthDay = arr[2];
    const dateOfBirthMonth =  arr[1];
    const dateOfBirthYear =  arr[0];

    $.ajax({
      url: checkout_object.ajax_url,
      type: 'POST',
      data: {
        action: 'check_equifax',
        fname: fname,
        lname: lname,
        address: address,
        city: city,
        province: province,
        zip: zip,
        phone: phone,
        language: language,
        dateOfBirthDay: dateOfBirthDay,
        dateOfBirthMonth: dateOfBirthMonth,
        dateOfBirthYear: dateOfBirthYear
      },
      success: function(response) {
        $('.modal').modal('show');
        $('.question-block').empty();
        console.log(response);
        if(response.status == 400){
          $( ".modale-error-block" ).text(" ");
          $( ".modale-error-block" ).append("Please check your data in fields"+'</br>');
          $.each(response.errors, function(key, value) {
            $(".modale-error-block").append(value+'</br>');
            console.log(value);
          });


        }else{
          $( ".modale-error-block" ).text(" ");
          $('.modale-form').css('display', 'none');
          var sel = $('.question-block');
          sel.append($('<label>TransactionID: '+response.transactionId+'</label></br>'));
          for (var i = 0, len = response.data.length ; i < len; i++) {}

          $.each(response.data, function(key, value) {
            console.log(key);
            $('.question-block').append(
              "<label id='45' >" + value.QuestionText + '</label></br>'
            );
            // var sel = $('.question-block');

            $.each(value.AnswerChoice, function(vv, op) {
              const radioButton = `<label><input 
                                  type='radio'
                                  class='user_ans'
                                  style='font-size: 10px;font-weight: normal'
                                  name='${value.questionId}'
                                  value='${op.answerId}'>${op._}</label></br>`;
              const $radio = $(radioButton);
              sel.append($radio);
            });
          });
          $('.question-block').append(
            $('<input type="submit" class="chack-answer pull-right">')
          );
          $(document).on('click', '.chack-answer', function(e) {
            e.preventDefault();
            const userAnswers = $('.question-block input[type=radio]:checked');
            let isCorrect = true;
            if (userAnswers.length !== response.data.length) {
              isCorrect = false;
            } else {

              var ques=[];
              var res =[{transactionId:response.transactionId,interactiveQueryId:response.interactiveQueryId,questions:ques}];
              // let interactiveQueryId ={"interactiveQueryId":response.interactiveQueryId};
              // res.push(transactionId);
              // res.push(interactiveQueryId);
              // res.push({"questions":ques});
              userAnswers.each(function() {
                console.log(this.name );
                console.log(this.value );
                const question = response.data.find((question) => question.questionId == this.name);
                const answer = question.AnswerChoice.find((item) => item.answerId == this.value);
                console.log(question);
                console.log(answer.correctAnswer);
                let obj= {
                  "questionId": this.value,
                  "answerId": this.name,
                };
                ques.push(obj);
                if (answer.correctAnswer === false) {
                  isCorrect = false;
                  return;
                }
              });
            }
            if(isCorrect == true){
              $( ".modale-error-block" ).text('');


              $.ajax({
                url: checkout_object.ajax_url,
                type: "POST",
                data: {
                  action: 'getIQAnswerRequest',
                  res:res
                },
                success: function (score) {
                  if(score==100){
                    checkout();
                    $('.question-block').remove();
                    $('.success').text('Checkout Success');
                  }
                  console.log(score);
                }
              })
            }else{
              $( ".modale-error-block" ).text( "Please, select the correct answer" );
            }

            console.log(isCorrect);
          });
        }
       }
    });
  }
  $('#submit_equifax').on('click', function(e) {

  });
  function checkout() {
    var mail = $('#email-check').val();
    var name = $('.name').val();
    var last_name = $('.last_name').val();
    var b_day = $('.b_day').val();
    var phone = $('.phone').val();
    var address = $('.address').val();
    var city = $('.city').val();
    var province = $('.province').val();
    var zip = $('.postal-code').val();
    var name_b = $('.name_b').val();
    var last_name_b = $('.last_name_b').val();
    var email_b = $('.email_b').val();
    var phone_b = $('.phone_b').val();
    var address_b = $('.address_b').val();
    var city_b = $('.city_b').val();
    var province_b = $('.province_b').val();
    var zip_b = $('.zip_b').val();
    var user_id = $('#id_user').val();

    var home_zip = $('.home_zip').val();
    var orders = $('.orders').val();
    var shiping_date = $('.shiping_date').val();
    var shiping_price = $('.shiping_price').val();
    $.ajax({
        url: checkout_object.ajax_url,
        type: "POST",
        data: {
            action: 'checkout_user_guest',
            user_id: user_id,
            mail: mail,
            name: name,
            last_name: last_name,
            b_day: b_day,
            phone: phone,
            address: address,
            city: city,
            province: province,
            zip: zip,
            name_b: name_b,
            last_name_b: last_name_b,
            email_b: email_b,
            phone_b: phone_b,
            address_b: address_b,
            city_b: city_b,
            province_b: province_b,
            zip_b: zip_b,
            home_zip: home_zip,
            orders: orders,
            shiping_date: shiping_date,
            shiping_price: shiping_price,

        },
        success: function () {
            console.log('ok');
            $('#email-check').val('');
            $('.name').val('');
            $('.last_name').val('');
            $('.b_day').val('');
            $('.phone').val('');
            $('.address').val('');
            $('.city').val('');
            $('.province').val('');
            $('.postal-code').val('');
            $('.name_b').val('');
            $('.last_name_b').val('');
            $('.email_b').val('');
            $('.phone_b').val('');
            $('.address_b').val('');
            $('.city_b').val('');
            $('.province_b').val('');
            $('.zip_b').val('');
            $('.home_zip').val('');
            $('.orders').val('');
             $('.shiping_date').val('');
             $('.shiping_type').val('');
           location.href = "home";
        }
    });
  }

})(jQuery);
