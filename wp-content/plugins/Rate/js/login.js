(function($) {
  $(window).bind('pageshow', function() {
    $('#zip').val('');
  });
  $('#register-form').on('submit', function(e) {
    e.preventDefault();
    var valid = '';
    var checked = '';
    var pass1 = $('#password').val();
    var pass2 = $('#confirm-password').val();
    $('.error').remove();
    var mail = $('#email').val();

    $.ajax({
      url: my_ajax_object.ajax_url,
      type: 'POST',
      data: {
        action: 'check_username',
        mail: mail
      },
      // dataType: "json",
      success: function(response) {
        $('.error_block').append('<div class="error">' + response + '</div>');
        valid = 'no';
      }
    });
    if($('#password').val().length >8){
        $('.error_block').append(
          '<div class="error">The password  must be 8  character </div>'
        );
        valid = 'no';
    }
    if (pass1 === pass2) {
      var bday = $('#date').val();
    } else {
      $('.error_block').append(
        '<div class="error">This field is Not matching</div>'
      );
      valid = 'no';
    }
    if (!$('.remember_me').is(':checked')) {
      checked = 'false';
    } else {
      checked = 'true';
    }

    if (!valid) {
      $.ajax({
        type: 'POST',
        // dataType: "json",
        // method : 'POST',
        url: my_ajax_object.ajax_url,
        data: {
          action: 'my_action',
          mail: mail,
          bday: bday,
          pass: pass1,
          checked: checked
        },
        success: function(msg) {
          console.log(msg);
        }
      });
      $('#email').val('');
      $('#date').val('');
      $('#password').val('');
      $('#confirm-password').val('');
      setTimeout(getprofile, 1000);
    }
  });

  var currencyTo = '';
  var currencyFrom = '';
  var sum = '';
  var flag;
  var flag_from;
  var input_class = '';
  var rate_class = '';

  flag = $('.currency-to-select option:selected').attr('data');
  $('.currency_flag').attr(
    'src',
    'https://www.countryflags.io/' + flag + '/flat/24.png'
  );
  // $('.flag_name_to').attr("value", flag);
  flag_from = $('.from-select option:selected').attr('dataa');
  $('.currency_from_flag').attr(
    'src',
    'https://www.countryflags.io/' + flag_from + '/flat/24.png'
  );

  $(document).on('change', '.from-select', function() {
    flag_from = $(this)
      .find(':selected')
      .attr('dataa');
    $(this)
      .prev()
      .attr('src', 'https://www.countryflags.io/' + flag_from + '/flat/24.png');

    currencyTo = $(this)
      .closest('.rate-value-block')
      .find('.currency-to-select')
      .find(':selected')
      .attr('data-to');
    console.log(currencyTo);

    currencyFrom = $(this)
      .find(':selected')
      .attr('data-from');
    console.log(currencyFrom);
    input_class = $(this)
      .closest('.rate-value-block')
      .find("[data='input']")
      .attr('class');
    console.log(input_class);
    sum = $(this)
      .closest('.rate-value-block')
      .find('.sum')
      .val();
    console.log(sum);
    input_class = $(this)
      .closest('.rate-value-block')
      .find("[data='input']")
      .attr('class');
    rate_class = $(this)
      .closest('.rate')
      .find("[data='rate-value']")
      .attr('class');
    console.log(rate_class);
    getCurrency();
  });

  $(document).on('change', '.currency-to-select', function() {
    flag = $(this)
      .find(':selected')
      .attr('data');
    $(this)
      .closest('.rate-value-block')
      .find('.flag_name_to')
      .attr('value', flag);
    $(this)
      .prev()
      .css('display', 'initial');
    $(this)
      .prev()
      .prev()
      .css('display', 'none');
    $(this)
      .prev()
      .attr('src', 'https://www.countryflags.io/' + flag + '/flat/24.png');
    currencyFrom = $(this)
      .closest('.rate-value-block')
      .find('.from-select')
      .find(':selected')
      .attr('data-from');
    console.log(currencyFrom);

    currencyTo = $(this)
      .find(':selected')
      .attr('data-to');
    console.log(currencyTo);
    sum = $(this)
      .closest('.rate-value-block')
      .find('.sum')
      .val();
    console.log(sum);
    input_class = $(this)
      .closest('.rate-value-block')
      .find("[data='input']")
      .attr('class');
    rate_class = $(this)
      .closest('.rate')
      .find("[data='rate-value']")
      .attr('class');
    console.log(rate_class);
    getCurrency();
  });

  $(document).on('keyup', '.sum', function(e) {
    sum = $(this).val();
    currencyFrom = $(this)
      .closest('.rate-value-block')
      .find('.from-select')
      .find(':selected')
      .attr('data-from');
    currencyTo = $(this)
      .closest('.rate-value-block')
      .find('.currency-to-select')
      .find(':selected')
      .attr('data-to');
    input_class = $(this)
      .closest('.rate-value-block')
      .find("[data='input']")
      .attr('class');
    rate_class = $(this)
      .closest('.rate')
      .find("[data='rate-value']")
      .attr('class');
    getCurrency();
  });
  if ($('.ivalue').val()) {
    var i = $('.ivalue').val();
  } else {
    i = 1;
  }


  $('.add-currency').click(function(e) {
    const div = $('#rate-block');
    const clone = div.clone().prop('id', 'rate-block-' + i);
    const input = clone.find('.have-to').attr('class', 'have-to-' + i);
    const asd = clone
      .find('.currency-to-select')
      .attr('data', 'select-to-' + i);
    const fromm = clone.find('.from-select').attr('data', 'select-from-' + i);
    const rate = clone.find('.rate-value').attr('class', 'rate-value-' + i);
    const cto = clone
      .find('.currency-to-select')
      .attr('name', 'order[' + i + '][cto]');
    const cfrom = clone
      .find('.from-select')
      .attr('name', 'order[' + i + '][cfrom]');
    const cflagto = clone
      .find('.flag_name_to')
      .attr('name', 'order[' + i + '][flag_name_to]');
    const sum = clone.find('.sum').attr('name', 'order[' + i + '][sum]');
    const total = clone
      .find('#have-to')
      .attr('name', 'order[' + i + '][total]');
    clone.find('.delete-btn').css('display', 'block');

    $('.cont-block').append(clone);
    i++;
  });

  $(document).on('click', '.rate .delete-btn', function(e) {
    $(this)
      .parent()
      .parent()
      .remove();
  });

  $('.sum').on('keypress keyup blur', function(event) {
    $(this).val(
      $(this)
        .val()
        .replace(/[^0-9\.]/g, '')
    );
    setTimeout(function(){
      if (
        (event.which !== 46 ||
          $(this)
            .val()
            .indexOf('.') !== -1) &&
        (event.which < 48 || event.which > 57)
      ) {
        event.preventDefault();
      }
    }, 7000);

  });
  $('#home_form_submit').click(function(e) {
    $('.error').remove();

    var zip = $('#zip')
      .val()
      .toUpperCase().substr(0, 3);
    console.log( zip.substr(0, 3));
    var all_zip = $('#zip-arr').val();
    if (all_zip.indexOf(zip) > -1) {
      console.log('isset');
      $.ajax({
        type: 'POST',
        url: my_ajax_object.ajax_url,
        dataType: 'json',
        data: {
          action: 'getShipping',
          zip: zip
        },
        success: function(response) {
          // console.log(response.same);
          console.log(response[0].same);
          $('#same').attr('data-price', response[0].same);
          $('#next').attr('data-price', response[0].next);
          $('#third').attr('data-price', response[0].third);
          $('.shipping').prop('disabled', false); // Element(s) are now enabled.
        }
      });
    } else {
      $('.reat_select').append(
        '<div class="error" style="margin-top: 20px">Our service in currently not available in your area, please sign up to our newsletters to be posted of future expansions</div>'
      );
      e.preventDefault();
    }

    var summ = 0;
    $('.sum').each(function() {
      summ += Number($(this).val());
    });
    if (summ > 2500) {
      $('.reat_select').append(
        '<div class="error" style="margin-top: 20px">The sum should not be more than 2500</div>'
      );

      e.preventDefault();
    }
    if (summ != '' && summ < 500) {
      $('.reat_select').append(
        '<div class="error" style="margin-top: 20px">The sum should be more than 500.</div>'
      );

      e.preventDefault();
    }
  });
  var status = $('.shipping option:selected').data('data-price');

  $('.shipping').change(function() {
    status = $(this)
      .find('option:selected')
      .attr('data-price');
    console.log(
      $(this)
        .find('option:selected')
        .attr('data-price')
    );
    $('#shipping_price').attr('value', status);
  });

  $('#zip').keyup(function() {
    $('.error').remove();
    var zip = $('#zip')
      .val()
      .toUpperCase().substr(0, 3);
    var all_zip = $('#zip-arr').val();
    if (all_zip.indexOf(zip) > -1) {
      console.log('isset');
      $.ajax({
        type: 'POST',
        url: my_ajax_object.ajax_url,
        dataType: 'json',
        data: {
          action: 'getShipping',
          zip: zip
        },
        success: function(response) {
          // console.log(response.same);
          console.log(response[0].same);
          $('#same').attr('data-price', response[0].same);
          $('#next').attr('data-price', response[0].next);
          $('#third').attr('data-price', response[0].third);
          $('.shipping').prop('disabled', false); // Element(s) are now enabled.
        }
      });
    } else {
      $('.reat_select').append(
        '<div class="error" style="margin-top: 20px">Our service in currently not available in your area, please sign up to our newsletters to be posted of future expansions</div>'
      );
      e.preventDefault();
    }
  });

  // $('#zip').keyup(function() {
  //
  // });
  function getCurrency() {
    if (currencyTo !== '' && currencyFrom !== '' && sum !== '') {
      $.ajax({
        type: 'POST',
        url: my_ajax_object.ajax_url,
        data: {
          action: 'getRate',
          currencyTo: currencyTo,
          currencyFrom: currencyFrom,
          sum: sum
        },
        success: function(response) {
          // document.getElementsByClassName(input_class)[0].innerHTML = response;
          document.getElementsByClassName(input_class)[0].value = response;
          console.log(response);
        }
      });
      $.ajax({
        type: 'POST',
        url: my_ajax_object.ajax_url,
        data: {
          action: 'getCurrency',
          currencyTo: currencyTo,
          currencyFrom: currencyFrom
        },
        success: function(response) {
          console.log(response);
          document.getElementsByClassName(rate_class)[0].innerHTML = response;
        }
      });
    }
  }

  var currencyTo_watch = '';
  var currencyFrom_watch = '';
  var flag_from_w = '';
  var flag_to = '';

  document.getElementById('rate-watch-name').innerHTML =
    '1' +
    $('.currency-from-watch')
      .find(':selected')
      .attr('dataa');
  flag_from_w = $('.currency-from-watch')
    .find(':selected')
    .attr('dataa');
  $('.currency-from-watch')
    .prev()
    .attr('src', 'https://www.countryflags.io/' + flag_from_w + '/flat/24.png');
  flag_to = $('.currency-to-watch')
    .find(':selected')
    .attr('data');
  $('.currency-to-watch')
    .prev()
    .attr('src', 'https://www.countryflags.io/' + flag_to + '/flat/24.png');
  currencyTo_watch = $('.currency-from-watch')
    .closest('.currency-block')
    .find('.currency-to-watch')
    .find(':selected')
    .attr('data-to');
  currencyFrom_watch = $('.currency-to-watch')
    .closest('.currency-block')
    .find('.currency-from-watch')
    .find(':selected')
    .attr('data-from');
  getCurrencySingle();

  $(document).on('change', '.currency-from-watch', function() {
    flag_from_w = $(this)
      .find(':selected')
      .attr('dataa');
    $(this)
      .prev()
      .attr(
        'src',
        'https://www.countryflags.io/' + flag_from_w + '/flat/24.png'
      );
    currencyTo_watch = $(this)
      .closest('.currency-block')
      .find('.currency-to-watch')
      .find(':selected')
      .attr('data-to');
    currencyFrom_watch = $(this)
      .find(':selected')
      .attr('data-from');
    getCurrencySingle();
  });

  $(document).on('change', '.currency-to-watch', function() {
    flag_to = $(this)
      .find(':selected')
      .attr('data');
    $(this)
      .prev()
      .attr('src', 'https://www.countryflags.io/' + flag_to + '/flat/24.png');
    currencyFrom_watch = $(this)
      .closest('.currency-block')
      .find('.currency-from-watch')
      .find(':selected')
      .attr('data-from');
    currencyTo_watch = $(this)
      .find(':selected')
      .attr('data-to');
    getCurrencySingle();
  });
  function getCurrencySingle() {
    if (currencyTo_watch !== '' && currencyFrom_watch !== '') {
      $.ajax({
        type: 'POST',
        url: my_ajax_object.ajax_url,
        // dataType: 'json',
        data: {
          action: 'getSingleRate',
          currencyTo: currencyTo_watch,
          currencyFrom: currencyFrom_watch
        },
        success: function(response) {
          var len = response.length;
          console.log(response);
          document.getElementById('rate-watch-name').innerHTML =
            '1' +
            $('.currency-from-watch')
              .find(':selected')
              .attr('dataa');
          document.getElementById('rate-watch').innerHTML = response;
        }
      });
    }
  }
  function getprofile() {
    var url = window.location + 'profile';
    $(location).attr('href', url);
  }
  $(window).bind('pageshow', function() {
    $('#zip').val('');
  });
  // if (window.history.go(-1)) {
  //   $('#zip').val('');
  //   alert('pop');
  // }
  // $(window).on('popstate', function(event) {
  //   $('#zip').val('');
  //   alert('pop');
  // });

})(jQuery);
