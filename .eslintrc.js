module.exports = {
  "extends": [
    "eslint:recommended"
  ],
  "installedESLint": true,
  "env": {
    "browser": true,
    "node": true
  },
  "globals": {
    "google": true,
    "angular": true,
    "$": true,
    "jQuery": true,
    "window": true,
    "Bloodhound": true,
    "Stripe": true
  },
  "rules": {
    "quotes": [
      "error",
      "single"
    ],
    "eqeqeq": "error",
    "strict": [
      "error",
      "safe"
    ],
    "indent": [
      "error",
      2
    ],
    "no-console": [
      "error",
      {"allow": ["log", "warn", "error"]}
    ],
    "no-alert": "off",
    "no-inline-comments": "error",
    "no-unused-vars": "warn",
    "max-len": [
      "error",
      80
    ],
    "semi": [
      "error",
      "always"
    ],
    "no-undef": "error",
    "space-before-function-paren": [
      "error",
      {"anonymous": "always", "named": "never"}
    ],
    "no-native-reassign": "off"
  }
};
